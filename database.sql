# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.25)
# Database: student_management
# Generation Time: 2021-08-09 22:33:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table auth_activation_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_activation_attempts`;

CREATE TABLE `auth_activation_attempts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table auth_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_groups`;

CREATE TABLE `auth_groups` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `auth_groups` WRITE;
/*!40000 ALTER TABLE `auth_groups` DISABLE KEYS */;

INSERT INTO `auth_groups` (`id`, `name`, `description`)
VALUES
	(1,'admin','Admin Group'),
	(2,'branch','Branch Group'),
	(3,'','');

/*!40000 ALTER TABLE `auth_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_groups_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_groups_permissions`;

CREATE TABLE `auth_groups_permissions` (
  `group_id` int unsigned NOT NULL DEFAULT '0',
  `permission_id` int unsigned NOT NULL DEFAULT '0',
  KEY `auth_groups_permissions_permission_id_foreign` (`permission_id`),
  KEY `group_id_permission_id` (`group_id`,`permission_id`),
  CONSTRAINT `auth_groups_permissions_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `auth_groups_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table auth_groups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_groups_users`;

CREATE TABLE `auth_groups_users` (
  `group_id` int unsigned NOT NULL DEFAULT '0',
  `user_id` int unsigned NOT NULL DEFAULT '0',
  KEY `auth_groups_users_user_id_foreign` (`user_id`),
  KEY `group_id_user_id` (`group_id`,`user_id`),
  CONSTRAINT `auth_groups_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `auth_groups_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `auth_groups_users` WRITE;
/*!40000 ALTER TABLE `auth_groups_users` DISABLE KEYS */;

INSERT INTO `auth_groups_users` (`group_id`, `user_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `auth_groups_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_logins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_logins`;

CREATE TABLE `auth_logins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int unsigned DEFAULT NULL,
  `date` datetime NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `auth_logins` WRITE;
/*!40000 ALTER TABLE `auth_logins` DISABLE KEYS */;

INSERT INTO `auth_logins` (`id`, `ip_address`, `email`, `user_id`, `date`, `success`)
VALUES
	(1,'::1','xehevawer@mailinator.com',4,'2021-07-24 13:52:09',1),
	(2,'::1','xehevawer@mailinator.com',4,'2021-07-26 13:26:14',1),
	(3,'::1','canomere@mailinator.com',5,'2021-07-26 13:57:08',1),
	(4,'::1','canomere@mailinator.com',5,'2021-07-30 09:48:04',1),
	(5,'::1','rovonuvy@mailinator.com',6,'2021-07-30 10:48:02',1),
	(6,'::1','canomere@mailinator.com',5,'2021-07-30 12:24:01',1),
	(7,'::1','rovonuvy@mailinator.com',6,'2021-07-30 14:37:17',1),
	(8,'::1','myjyt@mailinator.com',8,'2021-07-30 16:04:08',1),
	(9,'::1','myjyt@mailinator.com',8,'2021-07-31 03:24:45',1),
	(10,'::1','myjyt@mailinator.com',8,'2021-07-31 08:48:39',1),
	(11,'::1','myjyt@mailinator.com',8,'2021-07-31 16:09:55',1),
	(12,'::1','canomere@mailinator.com',5,'2021-07-31 17:26:33',1),
	(13,'::1','xehevawer@mailinator.com',4,'2021-07-31 18:02:11',1),
	(14,'::1','canomere@mailinator.com',5,'2021-07-31 18:03:01',1),
	(15,'::1','xehevawer@mailinator.com',4,'2021-08-01 09:38:27',1),
	(16,'::1','canomere@mailinator.com',5,'2021-08-01 09:39:06',1),
	(17,'::1','myjyt@mailinator.com',8,'2021-08-01 09:46:59',1),
	(18,'::1','canomere@mailinator.com',5,'2021-08-01 10:01:19',1),
	(19,'::1','myjyt@mailinator.com',8,'2021-08-01 10:13:17',1),
	(20,'::1','canomere@mailinator.com',5,'2021-08-03 09:03:15',1),
	(21,'::1','myjyt@mailinator.com',8,'2021-08-04 10:05:27',1),
	(22,'::1','tamecut@mailinator.com',NULL,'2021-08-04 10:09:59',0),
	(23,'::1','myjyt@mailinator.com',8,'2021-08-04 10:10:08',1),
	(24,'::1','canomere@mailinator.com',5,'2021-08-04 14:16:47',1),
	(25,'::1','canomere@mailinator.com',5,'2021-08-07 12:57:49',1),
	(26,'::1','canomere@mailinator.com',5,'2021-08-08 02:00:48',1),
	(27,'::1','canomere@mailinator.com',5,'2021-08-09 01:36:51',1),
	(28,'::1','canomere@mailinator.com',5,'2021-08-09 07:21:45',1),
	(29,'::1','myjyt@mailinator.com',8,'2021-08-09 09:33:31',1),
	(30,'::1','mucuzo@mailinator.com',9,'2021-08-09 10:41:55',1),
	(31,'::1','myjyt@mailinator.com',8,'2021-08-09 10:46:08',1),
	(32,'::1','canomere@mailinator.com',5,'2021-08-09 10:58:53',1),
	(33,'::1','canomere@mailinator.com',5,'2021-08-09 12:44:03',1),
	(34,'::1','canomere@mailinator.com',NULL,'2021-08-09 12:46:59',0),
	(35,'::1','canomere@mailinator.com',NULL,'2021-08-09 12:47:05',0),
	(36,'::1','canomere@mailinator.com',5,'2021-08-09 12:47:18',1),
	(37,'::1','canomere@mailinator.com',NULL,'2021-08-09 13:03:33',0),
	(38,'::1','canomere@mailinator.com',5,'2021-08-09 13:03:48',1),
	(39,'::1','canomere@mailinator.com',NULL,'2021-08-09 13:10:31',0),
	(40,'::1','susojineqy@mailinator.com',NULL,'2021-08-09 13:11:41',0),
	(41,'::1','xovurov@mailinator.com',7,'2021-08-09 13:11:58',1),
	(42,'::1','kyfydeha@mailinator.com',15,'2021-08-09 13:15:05',1),
	(43,'::1','canomere@mailinator.com',5,'2021-08-09 13:19:19',1),
	(44,'::1','admin@admin.com',1,'2021-08-09 17:27:20',1);

/*!40000 ALTER TABLE `auth_logins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_permissions`;

CREATE TABLE `auth_permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table auth_reset_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_reset_attempts`;

CREATE TABLE `auth_reset_attempts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table auth_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_tokens`;

CREATE TABLE `auth_tokens` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `selector` varchar(255) NOT NULL,
  `hashedValidator` varchar(255) NOT NULL,
  `user_id` int unsigned NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_tokens_user_id_foreign` (`user_id`),
  KEY `selector` (`selector`),
  CONSTRAINT `auth_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table auth_users_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_users_permissions`;

CREATE TABLE `auth_users_permissions` (
  `user_id` int unsigned NOT NULL DEFAULT '0',
  `permission_id` int unsigned NOT NULL DEFAULT '0',
  KEY `auth_users_permissions_permission_id_foreign` (`permission_id`),
  KEY `user_id_permission_id` (`user_id`,`permission_id`),
  CONSTRAINT `auth_users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `auth_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `auth_users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table branches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `branches`;

CREATE TABLE `branches` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` text,
  `phone_number` text,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `owners_name` varchar(255) NOT NULL,
  `owners_number` varchar(255) NOT NULL,
  `owners_picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text NOT NULL,
  `user_id` int unsigned NOT NULL,
  `wallet` int unsigned NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `branches_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `duration` int NOT NULL,
  `fees` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table exam_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exam_data`;

CREATE TABLE `exam_data` (
  `exam_id` int unsigned NOT NULL,
  `subject_id` int unsigned NOT NULL,
  `marks_obtained` int DEFAULT '0',
  `full_marks` int DEFAULT '100',
  KEY `exam_data_marks` (`exam_id`),
  CONSTRAINT `exam_data_marks` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;;



# Dump of table exams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exams`;

CREATE TABLE `exams` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int unsigned NOT NULL,
  `course_id` int unsigned NOT NULL,
  `exam_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `student_exam` (`student_id`),
  KEY `course_exam` (`course_id`),
  CONSTRAINT `course_exam` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `student_exam` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int NOT NULL,
  `batch` int unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`)
VALUES
	(2,'2017-11-20-223112','Myth\\Auth\\Database\\Migrations\\CreateAuthTables','default','Myth\\Auth',1627151793,1),
	(3,'2021-07-28-071946','App\\Database\\Migrations\\CreateStudentTable','default','App',1627507057,2),
	(4,'2021-07-28-131411','App\\Database\\Migrations\\CreateBranchTable','default','App',1627507457,3),
	(5,'2021-07-28-132848','App\\Database\\Migrations\\CreateCourseTable','default','App',1627507735,4),
	(6,'2021-07-28-132856','App\\Database\\Migrations\\CreateSubjectTable','default','App',1627507962,5);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table student_courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `student_courses`;

CREATE TABLE `student_courses` (
  `course_id` int unsigned NOT NULL,
  `subject_id` int unsigned NOT NULL,
  `student_id` int unsigned NOT NULL,
  KEY `student_course_id` (`student_id`),
  KEY `course` (`course_id`),
  KEY `subject` (`subject_id`),
  CONSTRAINT `course` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_course_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `subject` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;;



# Dump of table students
# ------------------------------------------------------------

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `father_name` text,
  `mother_name` text,
  `gender` enum('male','female','other') NOT NULL DEFAULT 'male',
  `dob` date NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `registration_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `roll_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `certificate_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade_obtained` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','continue','discontinue','terminated','completed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `branch_id` int unsigned DEFAULT NULL,
  `course_id` int unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `students_branch_id_foreign` (`branch_id`),
  KEY `student_course` (`course_id`),
  CONSTRAINT `student_branch` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `student_course` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table subjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` enum('theory','practical') NOT NULL DEFAULT 'practical',
  `course_id` int unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subjects_course` (`course_id`),
  CONSTRAINT `subjects_course` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `reset_hash` varchar(255) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `activate_hash` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `force_pass_reset` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `username`, `password_hash`, `reset_hash`, `reset_at`, `reset_expires`, `activate_hash`, `status`, `status_message`, `active`, `force_pass_reset`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'admin@admin.com','admin','$2y$10$vooyeRke9oV3ch8unVzfm.SgoMt9oLspUVaSb/iaDb.4zUDWBzR0K',NULL,NULL,'2021-08-09 14:20:40','2b7b3720c32efee329618f1db5ffe910',NULL,NULL,1,0,'2021-07-24 13:38:39','2021-08-09 13:20:40',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
