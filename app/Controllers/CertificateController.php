<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Helpers\Grades;
use App\Models\BranchModel;
use App\Models\StudentModel;
use Carbon\Carbon;

class CertificateController extends BaseController
{
    private $studentModel;
    private $db;
    public function __construct()
    {
        $this->studentModel = new StudentModel();
        $this->db = db_connect();
    }
    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        $this->db->close();
    }

    public function index()
	{
	    if(in_groups('branch')){
	        $branchModel = new BranchModel();
	        $branch_id = $branchModel->where('user_id', user_id())->first()->id;
            $students = $this->studentModel->getCompletedStudents($branch_id);
        }else{
            $students = $this->studentModel->getCompletedStudents();
        }


		return view('dashboard/certificate/view-certificate', ['students' => $students]);
	}
	public function generate()
    {
        $student = $this->studentModel->getForCertificate($this->request->getPostGet('student_id'));;
        $register_date = explode(' ',$student->registered_at)[0];
        $date = new Carbon($register_date);
        $start_date= $date->toFormattedDateString();
        $end_date = $date->addMonths($student->duration)->toFormattedDateString();

        $date2 = new Carbon($student->completed_at);

        return view('dashboard/certificate/preview-certificate', ['student' => $student, 'start_date'=>$start_date, 'end_date'=>$end_date, 'completed_at' => $date2->toDateString()]);
    }
}
