<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Helpers\Grades;
use App\Models\BranchModel;
use App\Models\CourseModel;
use App\Models\StudentModel;
use App\Models\SubjectModel;
use http\Env\Response;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;
use Myth\Auth\Password;

class AdminController extends BaseController
{
    private $studentModel;
    private $branchModel;
    private $courseModel;
    private $subjectModel;
    private $admin_model;
    private $db;
    public function __construct()
    {
        $this->studentModel = new StudentModel();
        $this->branchModel = new BranchModel();
        $this->courseModel = new CourseModel();
        $this->subjectModel = new SubjectModel();
        $this->admin_model = new UserModel();
        $this->db = db_connect();
    }
    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        $this->db->close();
    }

    private function checkIsAdmin(){
        if(in_groups('admin')){
            print_r("You are a admin");
        }
    }

    public function index()
	{
	    $total_students = $this->studentModel->countAll();
	    $total_branches = $this->branchModel->countAll();
	    $total_courses = $this->courseModel->countAll();
	    $total_subjects = $this->subjectModel->countAll();
	    $students = $this->studentModel
                        ->join('branches', 'branches.id=students.branch_id')
                        ->join('courses', 'courses.id=students.course_id')
                        ->select('students.name as name, courses.name as course_name, branches.name as branch_name, registration_number, roll_number, status')
                        ->orderBy('students.created_at', 'desc')
                        ->findAll(10);
        return view('dashboard/admin-index', [
            'students' => $students,
            'total_students' => $total_students,
            'total_branches' => $total_branches,
            'total_courses' => $total_courses,
            'total_subjects' => $total_subjects
        ]);
	}
	public function getStudents(){
        $students = $this->studentModel
                        ->join('branches', 'branches.id=students.branch_id')
                        ->join('courses', 'courses.id=students.course_id')
                        ->select('students.id as id, students.name as name, courses.name as course_name, branches.name as branch_name, registration_number, roll_number, status')
                        ->orderBy('students.created_at', 'desc')
                        ->findAll(10);
        return view('dashboard/student/admin-view-student', ['students' => $students]);
    }
    public function editStudent()
    {
        $student = $this->studentModel->getWithBranchAndCourse($this->request->getGet('id'));
        $exam_builder = $this->db->table('exams');
        $exam = $exam_builder->where('student_id', $this->request->getGet('id'))->where('course_id', $student->course_id)->get()->getFirstRow();
        if(!$exam){
            return view('dashboard/student/admin-edit-student', [
                'student' => $student,
            ]);
        }
        $exam_data_builder = $this->db->table('exam_data');

        $exam_data = $exam_data_builder
                    ->where('exam_id', $exam->id)
                    ->join('subjects', 'subject_id=subjects.id')
                    ->select('subjects.id as subject_id,subjects.name as subject_name, full_marks, marks_obtained')
                    ->get()->getResult();
        $grade = $student->grade_obtained;
        if(!isset($grade) || $grade == null){
            $grade = Grades::getGrades($this->request->getGet('id'), $student->course_id);
        }
        return view('dashboard/student/admin-edit-student', [
            'student' => $student,
            'id' => $exam->id,
            'exam_data' => $exam_data,
            'grade' => $grade,
        ]);
    }
    public function changeStudentStatus()
    {
        $certNumber = null;
        $grade = null;
        if($this->request->getPost('status') == 'completed'){
            $student_id = +$this->request->getPost('student_id');
            $student = $this->studentModel->find($student_id);
            $course = $this->courseModel->find($student->course_id);
            $courseAbb = $this->courseModel->getAbbreviation(explode(' ',$course->name));
            $certNumber = $courseAbb.'/'.str_pad($student->branch_id, 3, '0', STR_PAD_LEFT).''.$student->roll_number;
            $grade = Grades::getGrades($student_id, $student->course_id);
        }
        $q = $this->studentModel->save([
            'id' => +$this->request->getPost('student_id'),
            'status' => $this->request->getPost('status'),
            'certificate_number' => $certNumber,
            'grade_obtained' => $grade
        ]);

        if(!$q){
            return redirect()->back()->with('error', $this->studentModel->errors());
        }
        return redirect()->back()->with('message', 'Status Successfully Updated');
    }
    public function settings()
    {
        $admin = user();
        return view('dashboard/admin_settings/admin-profile', ['admin' => $admin]);
    }
    public function updatePassword()
    {
        $rules = [
            'new_password'     => 'required',
            'current_password' => 'required',
        ];
        if (! $this->validate($rules) )
        {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }
        if(($this->request->getPost('new_password') == $this->request->getPost('current_password'))){
            return redirect()->back()->withInput()->with('errors', ["Your new password cannot be similar to the old one"]);
        }
        if(user()->__get('password_hash') != $this->request->getPost('current_password')){
            return redirect()->back()->withInput()->with('errors', ["Provided old password does not match with the current password"]);
        }

        $user = new UserModel();
        $r = $user->save([
            'id' => user_id(),
            'password_hash' => Password::hash($this->request->getPost('new_password'))
        ]);
        if(!$r){
            return redirect()->back()->withInput()->with('errors', $user->errors());
        }
        return redirect()->back()->with('message', 'Status Successfully Updated');
    }

    public function getHash(){
        return Password::hash($this->request->getGet('password'));
    }

    public function frontendStudentFetch()
    {
        
        if(!isset($_GET['reg']) || !isset($_GET['name'])){
            return '<div class="alert alert-danger"> <p>Name and Registration number is required.</p> </div>';
        }
        $student_registration = $this->request->getPostGet('reg');
        $student = $this->studentModel
                        ->where('registration_number', $student_registration)
                        ->join('courses', 'courses.id=students.course_id')
                        ->join('branches' , 'branches.id=students.branch_id')
                        ->select('students.name as name, mother_name, father_name, certificate_number, grade_obtained, status, roll_number, registration_number,
                                        branches.code as branch_code, courses.name as course_name, courses.code as course_code')
                        ->first();
        if($student == null){
            return '<div class="alert alert-danger"> <p>No student Found</p> </div>';
        }
        $content = view('components/api_response', ['student' => $student]);
        header('Access-Control-Allow-Origin: website_url');
        header("Content-Type: application/json; charset=UTF-8");
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
        return $content;
    }
}
