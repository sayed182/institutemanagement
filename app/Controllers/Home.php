<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function __construct()
    {
        $this->config = config('App');
    }

    public function index()
	{
	    if(in_groups('admin')){
	        return redirect()->route('admin');
        }
        else if(in_groups('branch')){
            return redirect()->route('branch_view');
        }
        else{
            return redirect()->route('login');
        }
//		return view('dashboard/index');
	}


}
