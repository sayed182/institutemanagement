<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Entities\SubjectEntity;
use App\Models\CourseModel;
use App\Models\StudentModel;
use App\Models\SubjectModel;
use Carbon\Carbon;

class SubjectController extends BaseController
{
    private $courseModel;
    private $subjectModel;
    public function __construct(){
        $this->courseModel = new CourseModel();
        $this->subjectModel = new SubjectModel();
    }
	public function index()
	{
	    $subjects = $this->subjectModel->getSubjectsWithCourseCode();
        return view('dashboard/subject/view-subject', [
            'subjects' => $subjects
        ]);
	}
    public function add()
    {
        $courses = $this->courseModel->findAll();

        return view('dashboard/subject/add-subject', [
            'courses' => $courses,
        ]);
    }
    public function submit()
    {
        $subject = new SubjectEntity();
        $subject->fill($this->request->getPost());
        $data = $subject->toArray();
        if(! $this->subjectModel->save($data)){
            return redirect()->back()->withInput()->with('errors', $this->subjectModel->errors());
        }
        return redirect()->back()->with('message', 'Successfully saved the subject');
    }
    public function edit()
    {
        $courses = $this->courseModel->findAll();
        $subject = $this->subjectModel->find($this->request->getGet('id'));
        return view('dashboard/subject/edit-subject', [
                'courses' => $courses,
                'subject' => $subject,
        ]);
    }
    public function delete()
    {
        if(! $this->subjectModel->delete($this->request->getPost('id'))){
            return redirect()->back()->withInput()->with('errors', $this->subjectModel->errors());
        }
        return redirect()->back()->with('message', 'Successfully deleted the subject');
    }
}
