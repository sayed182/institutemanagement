<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Entities\StudentEntity;
use App\Models\BranchModel;
use App\Models\CourseModel;
use App\Models\StudentCourseModel;
use App\Models\StudentModel;
use App\Models\SubjectModel;
use Carbon\Carbon;

class StudentController extends BaseController
{
    private $courseModel;
    private $subjectModel;
    private $studentModel;
    private $branchModel;
    private $studentCourseModel;
    private $db;
    public function __construct()
    {
        $this->courseModel = new CourseModel();
        $this->subjectModel = new SubjectModel();
        $this->studentModel = new StudentModel();
        $this->branchModel = new BranchModel();
        $this->studentCourseModel = new StudentCourseModel();
        $this->db = db_connect();
    }
    public function __destruct()
    {
        $this->db->close();
    }

    public function index()
	{
        $branch = ($this->branchModel->where('user_id', user_id())->first());
	    $students = $this->studentModel->where('branch_id', $branch->id)->findAll();
        return view('dashboard/student/view-student', ['students' => $students]);
	}
    public function add()
    {
        $branch = ($this->branchModel->where('user_id', user_id())->first());
        $courses = $this->courseModel->findAll();
        $date = Carbon::now();
        $roll = str_pad($this->studentModel->countAll() + 1, '3', '0', STR_PAD_LEFT).'/'.$date->year;
        return view('dashboard/student/add-student',[
            'courses' => $courses,
            'branch_id' => $branch->id,
            'branch_code' => $branch->code,
            'roll' => $roll,
        ]);
    }

    public function submit()
    {
        
        $student = new StudentEntity();
        $student->fill($this->request->getPost());
        $data = $student->toArray();
        if(! $this->studentModel->save($data)){
            return redirect()->back()->withInput()->with('errors', $this->subjectModel->errors());
        }
//        $lastInsertId = ($this->studentModel->orderBy('created_at', 'desc')->first())->id;
//        $data['student_id']=$lastInsertId;
//        if(! $this->studentCourseModel->save($data)){
//            return redirect()->back()->withInput()->with('errors', $this->studentCourseModel->errors());
//        }
        return redirect()->back()->with('message', 'Successfully saved the student');
    }
    public function edit()
    {
        $courses = $this->courseModel->findAll();
        $student = $this->studentModel->getWithBranchAndCourse($this->request->getGet('id'));
        return view('dashboard/student/edit-student', [
            'student' => $student,
            'courses' => $courses,
        ]);
    }
    public function delete()
    {
        if(! $this->subjectModel->delete($this->request->getPost('id'))){
            return redirect()->back()->withInput()->with('errors', $this->subjectModel->errors());
        }
        return redirect()->back()->with('message', 'Successfully deleted the student');
    }
    public function addCoursesSubjectToStudent($studentId)
    {

    }

    public function getSubjects()
    {
        if(isset($_POST['id'])){
            $data =[];
            $subjects = $this->subjectModel->where('course_id',$_POST['id'])->findAll();
            foreach ($subjects as $subject) {
                array_push($data, ['id'=>$subject->id, 'name'=>$subject->name]);
            }
            return json_encode($data);
        }

    }

}
