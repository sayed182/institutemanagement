<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BranchModel;
use App\Models\StudentModel;
use Carbon\Carbon;

class ReportController extends BaseController
{
    private $db;
    private $reportModel;
    private $studentModel;
    public function __construct()
    {
        $this->db = db_connect();
        $this->reportModel = $this->db->table('exams');
        $this->studentModel = new StudentModel();
    }
    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        $this->db->close();
    }

    public function index()
    {
        if(in_groups('branch')){
            $branchModel = new BranchModel();
            $branch_id = $branchModel->where('user_id', user_id())->first()->id;
            $students = $this->studentModel->getCompletedStudents($branch_id);
        }else{
            $students = $this->studentModel->getCompletedStudents();
        }
        return view('dashboard/report/view-report', ['students' => $students]);
    }
    public function generate()
    {
        $student_id = $this->request->getGetPost('student_id');
        $student_data = $this->studentModel->getForCertificate($student_id);
        $register_date = explode(' ',$student_data->registered_at)[0];
        $date = new Carbon($register_date);
        $start_date= $date->toFormattedDateString();
        $end_date = $date->addMonths($student_data->duration)->toFormattedDateString();
        $data = $this->reportModel
                ->join('exam_data', 'exams.id=exam_data.exam_id')
                ->join('subjects', 'exam_data.subject_id=subjects.id')
                ->select('subjects.name, exam_data.full_marks, exam_data.marks_obtained')
                ->get()->getResult();
        $date2 = new Carbon($student_data->completed_at);
        return view('dashboard/report/preview-report', ['student'=> $student_data,'marks'=>$data, 'start_date' => $start_date, 'end_date'=>$end_date, 'completed_at'=>$date2->toDateString() ]);
    }
    public function submit()
    {
        if(!in_groups('admin')){
            return show_404();
        }
        $exam_table = $this->db->table('exam_data');
        $request = $this->request->getPost();
        $exam_id = $request['id'];
        for ($i = 0; $i < count($request['subject_id']); $i++){
            $result = $exam_table
                ->where('exam_id', $exam_id)
                ->where('subject_id', $request['subject_id'][$i])
                ->update([
                'full_marks' => $request['full_marks'][$i],
                'marks_obtained' => $request['marks_obtained'][$i],
            ]);
            if(!$result){
                return redirect()->back()->with('error', $this->db->error());
            }
        }
        return redirect()->back()->with('message', 'Marks Successfully Updated');
    }
}
