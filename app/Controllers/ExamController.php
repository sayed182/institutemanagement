<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Entities\StudentEntity;
use App\Entities\SubjectEntity;
use App\Models\CourseModel;
use App\Models\StudentModel;
use App\Models\SubjectModel;

class ExamController extends BaseController
{
    private $courseModel;
    private $studentModel;
    private $subjectModel;
    private $db;
    public function __construct()
    {
        $this->courseModel= new CourseModel();
        $this->studentModel = new StudentModel();
        $this->subjectModel = new SubjectModel();
        $this->db = db_connect();
    }
    public function __destruct()
    {
        $this->db->close();
    }

    public function index()
	{
	    $builder = $this->db->table('exams');

	    $students = $builder
                    ->join('students','exams.student_id=students.id')
                    ->join('courses', 'exams.course_id=courses.id')
                    ->select('students.id as student_id,students.name as name, students.registration_number, students.roll_number, courses.id as course_id, courses.name as course_name, exams.id as exam_id')
                    ->get()->getResult();
		return view('dashboard/exam/view-marks', ['students'=>$students]);
	}
	public function add()
    {
        $courses = $this->courseModel->findAll();
        return view('dashboard/exam/add-marks', ['courses'=>$courses]);
    }
    public function edit()
    {
        $builder = $this->db->table('exam_data');
        $exam_data = $builder->where('exam_id', $_GET['id'])
                            ->join('subjects', 'subject_id=subjects.id')
                            ->select('name, marks_obtained, full_marks')
                            ->get()->getResult();

        return view('dashboard/exam/edit-marks', ['exam_data'=>$exam_data]);
    }

    public function getStudents()
    {
        $student = $this->studentModel->where('course_id', $this->request->getPost('id'))->where('status', 'continue')->findAll();
        $studentEntity = new StudentEntity();
        $studentEntity->fill($student);
        return json_encode($studentEntity->toArray());
    }
    public function getSubject(){
        $subjects = $this->subjectModel->where('course_id', $this->request->getPost('id'))->findAll();
        $subjectEntity = new SubjectEntity();
        $subjectEntity->fill($subjects);
        return json_encode($subjectEntity->toArray());
    }
    public function studentMarks()
    {
        $student_id = $this->request->getPost('student_id');
        $course_id = $this->request->getPost('course_id');
        $subject_ids= $this->request->getPost('subject_id');
        $full_marks = $this->request->getPost('full_marks');
        $marks_obtained = $this->request->getPost('marks_obtained');
        $data=[];
        for($i=0; $i<count($subject_ids); $i++){
            $d['subject_id'] = $subject_ids[$i];
            $d['full_marks'] = $full_marks[$i];
            $d['marks_obtained'] = $marks_obtained[$i];
            array_push($data, $d);
        }
        $builder = $this->db->table('exams');
        $re = $builder->insert(['student_id'=>$student_id, 'course_id'=>$course_id]);
        if($re){
            $new_data=[];
            $exam_id = $this->db->query('select id from exams ORDER BY id DESC limit 1')->getFirstRow('array')['id'];
            foreach ($data as $d){

                $d['exam_id'] = $exam_id;
                array_push($new_data, $d);
            }
            $b = $this->db->table('exam_data');
            $b->insertBatch($new_data);
            return redirect()->back()->with('message', 'Successfully saved marks');
        }
        return redirect()->back()->with('error', 'There was an error saving marks. Please Try again or contact the admin.');
    }
}
