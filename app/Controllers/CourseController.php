<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Entities\CourseEntity;
use App\Models\CourseModel;

class CourseController extends BaseController
{
    private $courseModel;
    public function __construct()
    {
        $this->courseModel = new CourseModel();
    }

    public function index()
	{
	    $courses = $this->courseModel->findAll();
        return view('dashboard/course/view-course', ['courses'=> $courses]);
	}
    public function add()
    {
        return view('dashboard/course/add-course');
    }
    public function submit()
    {

        if (! $this->validate($this->courseModel->validationRules))
        {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }
        $course = new CourseEntity();
        $course->fill($this->request->getPost());
        if(! $this->courseModel->save($course->toArray())){
            return redirect()->back()->withInput()->with('errors', $this->courseModel->errors());
        }
        return redirect()->back()->withInput()->with('message', 'Action Performed Successfully');
    }
    public function edit()
    {
        $course = $this->courseModel->find($this->request->getGet('id'));
        return view('dashboard/course/edit-course', [
            'course' => $course,
        ]);
    }
    public function delete()
    {
        if(! $this->courseModel->delete($this->request->getPost('id'))){
            return redirect()->back()->withInput()->with('errors', $this->courseModel->errors());
        }
        return redirect()->back()->with('message', 'Successfully deleted the subject');
    }
}
