<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\BranchModel;
use App\Models\StudentModel;
use CodeIgniter\Session\Session;
use Myth\Auth\Config\Auth as AuthConfig;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;

class BranchController extends BaseController
{

    protected $auth;

    /**
     * @var AuthConfig
     */
    protected $authConfig;

    /**
     * @var Session
     */
    protected $session;
    private $studentsModel;
    private $branchModel;

    public function __construct()
    {
        // Most services in this controller require
        // the session to be started - so fire it up!
        $this->session = service('session');

        $this->authConfig = config('Auth');
        $this->auth = service('authentication');
        $this->studentsModel = new StudentModel();
        $this->branchModel = new BranchModel();
    }
	public function index()
	{
        $branches = $this->branchModel->findAll();
		return view('dashboard/branch/view-branch', ['branches' => $branches]);
	}
	public function add()
    {
        $branch = new BranchModel();
        $branch_count = $branch->countAll() + 1;
        return view('dashboard/branch/add-branch',['branch_count' => $branch_count]);
    }
    public function submit()
    {

        $users = model(UserModel::class);
        $branch = new BranchModel();

        // Validate basics first since some password rules rely on these fields
        $rules = [
            'branch-name' => 'required',
            'branch-phone' => 'required',
            'owners-name' => 'required',
            'owners-phone' => 'required',
            'address' => 'required',
            'username' => 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]',
            'email'    => 'required|valid_email|is_unique[users.email]',
        ];

        if (! $this->validate($rules))
        {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        // Validate passwords since they can only be validated properly here
        $rules = [
            'password'     => 'required|strong_password',
            'pass_confirm' => 'required|matches[password]',
        ];

        if (! $this->validate($rules))
        {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        }

        // Save the user
        $allowedPostFields = array_merge(['password'], $this->authConfig->validFields, $this->authConfig->personalFields);
        $user = new User($this->request->getPost($allowedPostFields));

        $this->authConfig->requireActivation === null ? $user->activate() : $user->generateActivateHash();
        $users = $users->withGroup('branch');
        $user_id = $users->insert($user, true);
        if($user_id) {
            $bname = $this->request->getPost('branch-name') ;
            $bemail = $this->request->getPost('email') ;
            $code = $this->request->getPost('username') ;
            $bphone = $this->request->getPost('branch-phone') ;
            $oname = $this->request->getPost('owners-name') ;
            $ophone = $this->request->getPost('owners-phone') ;
            $address = $this->request->getPost('address') ;
            $file = $this->request->getFile('owners_picture');
            $path = $file->store();
            $data = [
                'name' => $bname,
                'email' => $bemail,
                'phone_number' => $bphone,
                'code' => $code,
                'owners_name' => $oname,
                'owners_number' => $ophone,
                'address' => $address,
                'user_id' => $user_id,
                'owners_picture' => $path,
            ];

            if(! $branch->save($data)){
                return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
            }
            return redirect()->back()->withInput()->with('message', "Successfully saved branch");

        }
    }

    public function edit()
    {
        $branch = $this->branchModel->find($this->request->getGet('id'));
        return view('dashboard/branch/edit-branch', [
            'branch' => $branch,
        ]);
    }
    public function delete()
    {
        if(! $this->branchModel->delete($this->request->getPost('id'))){
            return redirect()->back()->withInput()->with('errors', $this->branchModel->errors());
        }
        return redirect()->back()->with('message', 'Successfully deleted the subject');
    }

    public function branchView()
    {
        $branch = $this->branchModel->where('user_id', user_id())->first();
        $total_students = $this->studentsModel->where('branch_id', $branch->id)->countAll();
        $wallet_balance = $branch->wallet;
        $total_courses = $this->studentsModel->select('count(distinct course_id) as total_courses')->first();
        $students = $this->studentsModel
                            ->where('branch_id', $branch->id)
                            ->join('courses', 'courses.id=students.course_id')
                            ->orderBy('students.created_at', 'desc')
                            ->select('students.name as name, courses.name as course_name, registration_number, roll_number, status')
                            ->findAll(10);
        return view('dashboard/index', [
            'total_students' => $total_students,
            'wallet_balance' => $wallet_balance,
            'total_courses' => $total_courses->total_courses,
            'students' => $students
        ]);
    }

    public function branchProfile()
    {
        $branch = $this->branchModel->where('user_id', user_id())->first();
        return view('dashboard/branch/branch-profile', ['branch'=>$branch]);
    }
}
