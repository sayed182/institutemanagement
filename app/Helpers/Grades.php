<?php
namespace App\Helpers;

class Grades{
    public static function getGrades( $student_id='' , $course_id = ''){

        $db = db_connect();

        $exam_builder = $db->table('exams');
        $exam = $exam_builder->where('student_id', $student_id)->where('course_id', $course_id)->get()->getFirstRow();
        $exam_data_builder =$db->table('exam_data');

        $exam_data = $exam_data_builder
            ->where('exam_id', $exam->id)
            ->join('subjects', 'subject_id=subjects.id')
            ->select('subjects.name as subject_name, full_marks, marks_obtained')
            ->get()->getResult();

        $total = array_sum(array_map(function($element){return $element->full_marks;}, $exam_data));
        $achieved = array_sum(array_map(function($element){return $element->marks_obtained;}, $exam_data));

        if($total<$achieved){
            throw new \Exception('Total Marks cannot be below marks obtained');
        }
        $marks = ceil($achieved/$total * 100);
        if($marks>0 && $marks < 30){
            return 'Retry';
        }
        elseif($marks>= 30 && $marks <50){
            return "C";
        }
        elseif ($marks >= 50 && $marks < 60 ){return 'B';}
        elseif ($marks >= 60 && $marks <80 ){return 'A';}
        elseif ($marks >= 80 ){return  'A+';}
    }
}