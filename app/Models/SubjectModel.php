<?php

namespace App\Models;

use CodeIgniter\Model;

class SubjectModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'subjects';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'App\Entities\SubjectEntity';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['name', 'code', 'author', 'type', 'course_id'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getSubjectsWithCourseCode(){
		return $this->join('courses', 'courses.id=subjects.course_id')
					->select('subjects.name, subjects.code, subjects.author, subjects.type, courses.name as course_name, courses.code course_code')->findAll();
	}
}
