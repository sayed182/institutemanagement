<?php

namespace App\Models;

use CodeIgniter\Model;

class StudentModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'students';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'App\Entities\StudentEntity';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['name', 'father_name', 'mother_name', 'gender', 'dob', 'phone_number', 'email', 'address','certificate_number','grade_obtained', 'branch_id','registration_number', 'roll_number', 'status', 'course_id' ];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getWithBranchAndCourse($id){
	   return $this->join('branches', 'branch_id=branches.id')
                ->join('courses', 'courses.id=students.course_id', 'right join')
                ->select('students.id as id, students.name as name, students.email, students.address, students.dob, students.phone_number, courses.id as course_id, courses.name as course_name, courses.code as course_code, branches.name as branch_name, branches.code as branch_code, registration_number, roll_number, status')
                ->where('students.id', $id)
                ->first();
    }

    public function getForCertificate($id){
        return $this->join('branches', 'branch_id=branches.id')
            ->join('courses', 'courses.id=students.course_id', 'right join')
            ->join('exams', 'exams.student_id=students.id', 'right join')
            ->select('students.id as id, students.name as name, students.email, students.mother_name, students.father_name, students.phone_number,  students.created_at as registered_at, students.updated_at as completed_at, registration_number, roll_number, status, students.certificate_number, students.grade_obtained,
                            courses.id as course_id,courses.duration as course_duration, courses.name as course_name, 
                            branches.name as branch_name,exams.id as exam_id ')
            ->where('students.id', $id)
            ->first();
    }

    public function getCompletedStudents($branch_id=null){
	    $query = $this->join('branches', 'branch_id=branches.id')
            ->join('courses', 'courses.id=students.course_id', 'right join')
            ->select('students.id as id, students.name as name, students.email, students.address, students.dob, students.phone_number, students.grade_obtained , registration_number, roll_number, status,
            courses.id as course_id, courses.name as course_name, 
            branches.id as branch_id,branches.name as branch_name')
            ->where('students.status', 'completed');
	    if($branch_id != null){
	        $query = $query->where('branches.id', $branch_id);
        }
        return $query->findAll();
    }
}
