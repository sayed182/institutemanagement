<?php

namespace App\Models;

use CodeIgniter\Model;

class CourseModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'courses';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
    protected $returnType           = 'App\Entities\CourseEntity';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['name', 'code', 'duration', 'fees'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
	    'name'=>'required|max_length[20]',
        'code'=>'required|max_length[20]',
        'duration'=> 'required|numeric',
        'fees' => 'required|numeric'
    ];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getAbbreviation($name){
	    $abb = "";
	    foreach($name as $data){
	        $abb .= strtoupper($data[0]);
        }
	    return $abb;
    }
}
