<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateBranchTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'email' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'phone_number' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'branch_code' => [
                'type' => 'VARCHAR',
                'constraint'     => '255',
            ],
            'owners_name' => [
                'type' => 'VARCHAR',
                'constraint'     => '255',
            ],
            'owners_number' => [
                'type' => 'VARCHAR',
                'constraint' => '255'
            ],
            'address' => [
                'type' => 'TEXT',
            ],
            'user_id' => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
            'created_at'       => ['type' => 'datetime', 'null' => true],
            'updated_at'       => ['type' => 'datetime', 'null' => true],
            'deleted_at'       => ['type' => 'datetime', 'null' => true],

        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('user_id','users','id','CASCADE','CASCADE');
        $this->forge->createTable('branches');
	}

	public function down()
	{
		$this->forge->dropTable('branches');
	}
}
