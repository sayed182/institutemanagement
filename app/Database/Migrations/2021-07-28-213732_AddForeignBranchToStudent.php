<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddForeignBranchToStudent extends Migration
{
	public function up()
	{
	    $this->forge->addColumn('students', [
	       'branch_id' => [
	           'type' => 'INT',
               'constraints' => 5,
               'unsigned' => true
           ]
        ]);
		$this->forge->addForeignKey('branch_id', 'branches', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->forge->dropForeignKey('students', 'branch_id');
		$this->forge->dropColumn('students','branch_id');
	}
}
