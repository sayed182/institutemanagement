<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateStudentTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'father_name' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'mother_name' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'gender' => [
                'type' => 'ENUM',
                'constraint'     => ['male', 'female', 'other'],
                'default'        => 'male',
            ],
            'dob' => [
                'type' => 'DATE',
            ],
            'phone_number' => [
                'type' => 'VARCHAR',
                'constraint' => '255'
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '255'
            ],
            'address' => [
                'type' => 'TEXT',
            ],
            'created_at'       => ['type' => 'datetime', 'null' => true],
            'updated_at'       => ['type' => 'datetime', 'null' => true],
            'deleted_at'       => ['type' => 'datetime', 'null' => true],

        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('students');
	}

	public function down()
	{
		$this->forge->dropTable('students');
	}
}
