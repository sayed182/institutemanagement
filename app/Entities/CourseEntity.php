<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class CourseEntity extends Entity
{
//    public $name;
//    public $code;
//    public $duration;
//    public $fees;
    protected $attributes = [
        'id' => null,
        'course_name' => null,        // Represents a username
        'course_code' => null,
        'course_duration' => null,
        'course_fees'     => null,
        'created_at' => null,
        'updated_at' => null,
    ];
	protected $datamap = [];
	protected $dates   = [
		'created_at',
		'updated_at',
		'deleted_at',
	];
	protected $casts   = [];
}
