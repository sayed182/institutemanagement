<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(true);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('forgot-view', function(){
    return view('Auth/reset');
});
$routes->get('get-hash', 'AdminController::getHash');
$routes->get('verifystudent', 'AdminController::frontendStudentFetch');
$routes->group('admin',['filter' => 'role:admin'], function ($routes){
    $routes->get('/', 'AdminController::index');
    $routes->group('branch', function ($routes){
        $routes->get('/', 'BranchController::index', ['as'=> 'all_branch']);
        $routes->get('add', 'BranchController::add', ['as'=> 'get_add_branch']);
        $routes->post('add', 'BranchController::submit', ['as'=> 'post_add_branch']);
        $routes->get('edit', 'BranchController::edit', ['as'=> 'edit_branch']);
        $routes->post('delete', 'BranchController::delete', ['as'=> 'delete_branch']);
    });
    $routes->group('students', function ($routes){
        $routes->get('/', 'AdminController::getStudents', ['as'=> 'admin_all_student']);
        $routes->get('add', 'StudentController::add', ['as'=> 'admin_get_add_student']);
        $routes->post('add', 'StudentController::submit', ['as'=> 'admin_post_add_student']);
        $routes->get('edit', 'AdminController::editStudent', ['as'=> 'admin_edit_student']);
        $routes->post('change_student_status', 'AdminController::changeStudentStatus', ['as'=> 'admin_change_student_status']);
        $routes->post('delete', 'StudentController::delete', ['as'=> 'delete_student']);
    });
    $routes->group('course', function ($routes){
        $routes->get('/', 'CourseController::index', ['as'=> 'all_course']);
        $routes->get('add', 'CourseController::add', ['as'=> 'get_add_course']);
        $routes->post('add', 'CourseController::submit', ['as'=> 'post_add_course']);
        $routes->get('edit', 'CourseController::edit', ['as'=> 'edit_course']);
        $routes->post('delete', 'CourseController::delete', ['as'=> 'delete_course']);
    });
    $routes->group('subject', function ($routes){
        $routes->get('/', 'SubjectController::index', ['as'=> 'all_subjects']);
        $routes->get('add', 'SubjectController::add', ['as'=> 'get_add_subject']);
        $routes->post('add', 'SubjectController::submit', ['as'=> 'post_add_subject']);
        $routes->get('edit', 'SubjectController::edit', ['as'=> 'edit_subject']);
        $routes->post('delete', 'SubjectController::delete', ['as'=> 'delete_subject']);
    });

    $routes->group('settings', function ($routes){
        $routes->get('/', 'AdminController::settings', ['as'=> 'admin_settings']);
        $routes->get('add', 'AdminController::add_admin', ['as'=> 'admin_get_add_admin']);
        $routes->post('updatePassword', 'AdminController::updatePassword', ['as' => 'admin_update_password']);
    });
});

$routes->group('branch', ['filter' => 'role:branch'], function ($routes){
    $routes->get('/','BranchController::branchView', ['as' => 'branch_view']);
    $routes->get('profile', 'BranchController::branchProfile', ['as' => 'branch_profile']);
    $routes->group('student', function ($routes){
        $routes->get('/', 'StudentController::index', ['as'=> 'all_student']);
        $routes->get('add', 'StudentController::add', ['as'=> 'get_add_student']);
        $routes->post('add', 'StudentController::submit', ['as'=> 'post_add_student']);
        $routes->get('edit', 'StudentController::edit', ['as'=> 'edit_student']);
        $routes->post('delete', 'StudentController::delete', ['as'=> 'delete_student']);
        $routes->post('get_subjects_by_id', 'StudentController::getSubjects', ['as'=> 'get_subjects']);
    });
    $routes->group('exam', function ($routes){
        $routes->get('/', 'ExamController::index', ['as'=>'all_exam']);
        $routes->get('add', 'ExamController::add', ['as'=>'get_add_exam']);
        $routes->get('edit', 'ExamController::edit', ['as'=>'edit_exam']);
        $routes->post('submit_marks', 'ExamController::studentMarks', ['as'=>'submit_marks']);
        $routes->post('get_student_by_course_id', 'ExamController::getStudents', ['as'=> 'get_students_by_course']);
    });
});

$routes->group('certificate', ['filter' => 'role:admin,branch'], function($routes){
    $routes->get('/', 'CertificateController::index', ['as'=> 'all_certificates']);
    $routes->get('add', 'CertificateController::add', ['as'=> 'get_add_subject']);
    $routes->post('add', 'CertificateController::submit', ['as'=> 'post_add_subject']);
    $routes->get('edit', 'CertificateController::edit', ['as'=> 'edit_subject']);
    $routes->post('delete', 'CertificateController::delete', ['as'=> 'delete_subject']);
    $routes->post('generate', 'CertificateController::generate', ['as'=> 'generate_certificate']);
});
$routes->group('report',['filter' => 'role:admin,branch'], function($routes){
    $routes->get('/', 'ReportController::index', ['as'=> 'all_report']);
    $routes->get('add', 'ReportController::add', ['as'=> 'get_add_report']);
    $routes->post('add', 'ReportController::submit', ['as'=> 'post_add_report']);
    $routes->get('edit', 'ReportController::edit', ['as'=> 'edit_report']);
    $routes->post('delete', 'ReportController::delete', ['as'=> 'delete_report']);
    $routes->post('generate', 'ReportController::generate', ['as'=> 'generate_report']);
});




/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
