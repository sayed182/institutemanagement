<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-text mx-3">CAAT Dashboard</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?= route_to('branch_view'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">




    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= base_url('/student/')?>" data-toggle="collapse" data-target="#collapseStudent"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-user"></i>
            <span>Student</span>
        </a>
        <div id="collapseStudent" class="collapse" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= route_to('get_add_student')?>">Add Student</a>
                <a class="collapse-item" href="<?= route_to('all_student')?>">View All Students</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= base_url('/student/')?>" data-toggle="collapse" data-target="#collapseExam"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-clipboard"></i>
            <span>Exam</span>
        </a>
        <div id="collapseExam" class="collapse" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= route_to('get_add_exam')?>">Add Marks</a>
                <a class="collapse-item" href="<?= route_to('all_exam')?>">View Students Marks</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="<?= route_to('all_certificates')?>"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-graduation-cap"></i>
            <span>Certificates</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?= route_to('all_report')?>">
            <i class="fas fa-fw fa-bookmark"></i>
            <span>Report Cards</span>
        </a>
    </li>



    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Settings
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= route_to('branch_profile')?>"
           aria-expanded="false" aria-details="branch profile page">
            <i class="fas fa-fw fa-folder"></i>
            <span>Account</span>
        </a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->