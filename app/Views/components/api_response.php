<style>
    .completed{
        color: green;
    }
    .terminated{
        color: red;
    }
    .discontinue{
        color: darkorange;
    }
    .pending{
        color: yellow;
    }
</style>
    <div class="row">
        <div class="col-md-4">
            Name of the Student :
        </div>
        <div class="col-md-8"><?= $student->name; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            Father’s Name:
        </div>
        <div class="col-md-8"><?= $student->father_name?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            Mother’s Name:
        </div>
        <div class="col-md-8"> <?= $student->mother_name?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            Institute Code:
        </div>
        <div class="col-md-8"><?= $student->branch_code?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            Name of the Course:
        </div>
        <div class="col-md-8"><?= $student->course_name?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            Regd. No:
        </div>
        <div class="col-md-8"><?= $student->registration_number?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-4">
            Roll No:
        </div>
        <div class="col-md-8"><?= $student->roll_number?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            Certificate SL No:
        </div>
        <div class="col-md-8"><?= $student->certificate_number; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            Your Status :
        </div>
        <div class="col-md-8 <?= $student->status;?>">
            <span style="font-weight: bold;"><?= $student->status; ?></span>
        </div>
    </div>

