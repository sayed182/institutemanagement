<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-text mx-3">CAAT Dashboard</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('admin')?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= base_url('/branch/')?>" data-toggle="collapse" data-target="#collapseBranch"
           aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-code-branch"></i>
            <span>Branch</span>
        </a>
        <div id="collapseBranch" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= route_to('get_add_branch'); ?>">Add Branch</a>
                <a class="collapse-item" href="<?= route_to('all_branch'); ?>">View All Branches</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= route_to('all_student');?>" data-toggle="collapse" data-target="#collapseStudent"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-book-reader"></i>
            <span>Student</span>
        </a>
        <div id="collapseStudent" class="collapse" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= route_to('admin_all_student');?>">View All Students</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= route_to('all_course');?>" data-toggle="collapse" data-target="#collapseCourse"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-clipboard"></i>
            <span>Course</span>
        </a>
        <div id="collapseCourse" class="collapse" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= route_to('get_add_course')?>">Add Course</a>
                <a class="collapse-item" href="<?= route_to('all_course')?>">View All Course</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="<?= route_to('all_subjects')?>" data-toggle="collapse" data-target="#collapseSubject"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-book-medical"></i>
            <span>Subject</span>
        </a>
        <div id="collapseSubject" class="collapse" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= route_to('get_add_subject')?>">Add Subject</a>
                <a class="collapse-item" href="<?= route_to('all_subjects')?>">View All Subject</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="<?= route_to('all_certificates')?>"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-graduation-cap"></i>
            <span>Certificates</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?= route_to('all_report')?>">
            <i class="fas fa-fw fa-bookmark"></i>
            <span>Report Cards</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading" >
        SETTINGS

    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link" href="<?= route_to('admin_settings')?>">
            <i class="fas fa-fw fa-cogs"></i>
            <span>Settings</span>
        </a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->