<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

<div class="container main">
    <?= $this->include('Views/components/_message_block') ?>
    <?php if(false){?>
    <div class="card w-75 mx-auto o-hidden border-0 shadow-lg my-3">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-3">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Select Student</h1>
                        </div>
                        <form class="user" action="#" method="post" id="subjectSelect">
                            <?= csrf_field() ?>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <select type="text" class="form-control rounded-pill" id="courseName" placeholder="Subject Course" name="course_id" required>
                                        <option value="">Please select a course</option>
                                        <?php foreach ($courses as $course) { ?>
                                            <option value="<?= $course->id; ?>"><?= $course->name; ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select type="text" class="form-control rounded-pill" id="student" placeholder="Select Student" name="student" required>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-user btn-block" id="loadSubjects">
                                Load Subjects
                            </button>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php }else{?>
    <div class="col-md-8 col-sm-12 offset-md-2">
        <div class="card card-primary">
            <div class="card-header bg-success">
                <h5 class="card-title">Student Select</h5>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="user" action="#" method="post" id="subjectSelect">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3 d-flex align-items-center">
                            <label for="courseName">Select Course</label>
                        </div>
                        <div class="col-9">
                            <select type="text" class="form-control" id="courseName" placeholder="Subject Course" name="course_id" required>
                                <option value="">Please select a course</option>
                                <?php foreach ($courses as $course) { ?>
                                    <option value="<?= $course->id; ?>"><?= $course->name; ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    
                    <div class="row mt-4">
                        <div class="col-3 d-flex align-items-center">
                            <label for="student">Select Student</label>
                        </div>
                        <div class="col-9">
                            <select type="text" class="form-control" id="student" placeholder="Select Student" name="student" required>
                            </select>
                        </div>
                    </div>
                </div>
                <?= csrf_field() ?>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" id="loadSubjects">
                        Load Subjects
                    </button>
                </div>



            </form>
        </div>
    </div>
    <?php }?>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">All Subjects</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <form action="<?= route_to('submit_marks') ?>" method="post" id="my-form">
                    <input type="hidden" name="course_id" id="course_id">
                    <input type="hidden" name="student_id" id="student_id">
                </form>
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Subject</th>
                            <th>Full Marks</th>
                            <th>Marks Obtained</th>
                        </tr>
                    </thead>

                    <tbody id="subjectsContainer">
                        <tr>
                            <td class="w-50">Example Subject</td>
                            <td><input type="number" class="w-100" name="" id="" disabled></td>
                            <td><input type="number" class="w-100" name="" id="" disabled></td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

</div>

<?= $this->endSection() ?>

<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('scripts') ?>

<script>
    $("#courseName").on('change', function(event) {
        console.log(event.target.value);
        $.ajax({
            type: 'post',
            url: "<?= route_to('get_students_by_course') ?>",
            data: {
                'id': event.target.value
            },
            success: function(response) {
                var data = JSON.parse(response);
                var container = $("#student");
                if(data.length <=0 ){
                    container.html(`
                        <option value="#">No Students Found</option>
                    `);
                    $("#loadSubjects").attr('disabled',true);
                }
                data.forEach((element) => {
                    container.append(`
                        <option value="${element.id}">${element.name}</option>
                    `)
                })
            },
            error: function(error) {
                console.log(error);
            }
        })
    });
    $("#loadSubjects").on('click', function(event) {
        event.preventDefault();
        if (!$("#subjectSelect")[0].reportValidity()) {
            return false;
        }
        $.ajax({
            type: 'post',
            url: "<?= route_to('get_subjects') ?>",
            data: {
                'id': $("#courseName").val()
            },
            success: function(response) {
                $('#course_id').val($("#courseName").val())
                $('#student_id').val($("#student").val())
                var data = JSON.parse(response);
                var container = $("#subjectsContainer");
                container[0].innerHTML = "";
                data.forEach((element) => {
                    container.append(`
                        <tr data-subject="${element.id}" class="exam-subject">
                            <td class="w-50"><input type="hidden" name="subject_id[]" id="" form="my-form" value="${element.id}">${element.name}</td>
                            <td ><input type="number"  class="w-100" name="full_marks[]" id="" form="my-form" placeholder="0"></td>
                            <td ><input type="number" class="w-100" name="marks_obtained[]" id="" form="my-form" placeholder="0"></td>
                        </tr>
                    `)
                });
                container.append(`
                        <tr >
                            <td class="w-50">
                                <input type="checkbox" name="confirm_submit" form="my-form" id="confirm_submit" required>
                                <label for="confirm_submit">I have checked the entered marks.</label>
                            </td>
                            <td ></td>
                            <td ><button class="btn btn-block btn-success" form="my-form" id="submitMarks">Submit Marks</button></td>
                        </tr>
                    `)
            },
            error: function(error) {
                console.log(error);
            }
        });

    })
</script>
<?= $this->endSection(); ?>