<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

    <div class="container">
        <?= $this->include('Views/components/_message_block') ?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Subjects</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <form action="<?= route_to('submit_marks')?>" method="post" id="my-form">
                        <input type="hidden" name="course_id" id="course_id">
                        <input type="hidden" name="student_id" id="student_id">
                    </form>
                    <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Subject</th>
                            <th>Full Marks</th>
                            <th>Marks Obtained</th>
                        </tr>
                        </thead>

                            <tbody id="subjectsContainer">
                            <?php if(!empty($exam_data)){
                                foreach ($exam_data as $data){?>
                                    <tr>
                                        <td class="w-50"><?= $data->name; ?></td>
                                        <td ><input type="number" class="w-100" name="" id="" disabled value="<?= $data->full_marks; ?>"></td>
                                        <td ><input type="number" class="w-100" name="" id="" disabled value="<?= $data->marks_obtained; ?>"></td>
                                    </tr>
                                <?php }
                            }?>

                            </tbody>

                    </table>
                </div>
            </div>
        </div>

    </div>

<?= $this->endSection() ?>

<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('scripts') ?>

<script>
    //$("#courseName").on('change', function (event){
    //    console.log(event.target.value);
    //    $.ajax({
    //        type:'post',
    //        url: "<?//= route_to('get_students_by_course')?>//",
    //        data:{'id': event.target.value},
    //        success: function (response){
    //            var data = JSON.parse(response);
    //            var container = $("#student");
    //            data.forEach((element)=>{
    //                container.append(`
    //                    <option value="${element.id}">${element.name}</option>
    //                `)
    //            })
    //        },
    //        error: function (error){
    //            console.log(error);
    //        }
    //    })
    //});
    $("#loadSubjects").on('click', function (event){
        event.preventDefault();
        if(!$("#subjectSelect")[0].reportValidity()){
            return false;
        }
        $.ajax({
            type:'post',
            url: "<?= route_to('get_subjects')?>",
            data:{'id': $("#courseName").val()},
            success: function (response){
                $('#course_id').val($("#courseName").val())
                $('#student_id').val($("#student").val())
                var data = JSON.parse(response);
                var container = $("#subjectsContainer");
                container[0].innerHTML = "";
                data.forEach((element)=>{
                    container.append(`
                        <tr data-subject="${element.id}" class="exam-subject">
                            <td class="w-50"><input type="hidden" name="subject_id[]" id="" form="my-form" value="${element.id}">${element.name}</td>
                            <td ><input type="number"  class="w-100" name="full_marks[]" id="" form="my-form" placeholder="0"></td>
                            <td ><input type="number" class="w-100" name="marks_obtained[]" id="" form="my-form" placeholder="0"></td>
                        </tr>
                    `)
                });
                container.append(`
                        <tr >
                            <td class="w-50"></td>
                            <td ></td>
                            <td ><button class="btn btn-block btn-success" form="my-form" id="submitMarks">Submit Marks</button></td>
                        </tr>
                    `)
            },
            error: function (error){
                console.log(error);
            }
        })
    })
</script>
<?= $this->endSection();?>
