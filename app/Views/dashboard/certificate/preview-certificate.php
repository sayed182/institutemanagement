<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Certificate</title>
    <style>
        * {
            -webkit-print-color-adjust: exact !important;   /* Chrome, Safari, Edge */
            color-adjust: exact !important;                 /*Firefox*/
        }
        body {
            background: rgb(204,204,204);
        }
        span{
            text-transform: uppercase;
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
            font-family: "Nunito", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-weight: bold;
            position: relative;
            size: A4;
        }
        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
            background-image: url("<?= base_url('img/certificate_template/0001.jpg')?>");
            background-size: contain;
        }
        .text{
            position: absolute;
            text-transform: uppercase;
        }
        .primary-text{
            font-size: 1.2rem;
        }
        #student-roll{
            top: 19px;
            left: 120px;
        }
        #student-registration{
            top: 19px;
            right: 100px;
        }
        #student-name{
            top: 460px;
            left: 50%;
            transform: translateX(-50%);
        }
        #student-mother-name{
            top: 500px;
            left: 50%;
            transform: translateX(-50%);
        }
        #student-father-name{
            top: 540px;
            left: 50%;
            transform: translateX(-50%);
        }
        #course-duration{
            top: 580px;
            left: 64%;
            transform: translateX(-50%);
        }
        #course-name{
            width: 72%;
            top: 615px;
            left: 62%;
            transform: translateX(-50%);
        }
        #branch-name{
            width: 72%;
            top: 660px;
            left: 62%;
            transform: translateX(-50%);
        }
        #course-start{
            top: 700px;
            left: 32%;
            transform: translateX(-50%);
        }
        #course-end{
            top: 700px;
            left: 80%;
            transform: translateX(-50%);
        }
        #exam-grade{
            top: 740px;
            left: 50%;
            transform: translateX(-50%);
        }
        #certificate-number{
            top: 915px;
            left: 140px;
        }
        #certificate-date{
            top: 945px;
            left: 150px;
        }
        @media print {
            body * {
                visibility: hidden;
            }
            page[size="A4"], .text{
                visibility: visible;
                margin: 0 !important;
            }

        }
    </style>

</head>
<body>
<page size="A4" id="content">
    <span class="text sub-text" id="student-roll"><?= $student->roll_number; ?></span>
    <span class="text sub-text" id="student-registration"><?= $student->registration_number; ?></span>
    <span class="text primary-text" id="student-name"><?= $student->name; ?></span>
    <span class="text primary-text" id="student-mother-name"><?= $student->mother_name; ?></span>
    <span class="text primary-text" id="student-father-name"><?= $student->father_name; ?></span>
    <span class="text primary-text" id="course-duration"><?= $student->course_duration?> Months</span>
    <span class="text primary-text" id="course-name"><?= $student->course_name?></span>
    <span class="text primary-text" id="branch-name"><?= $student->branch_name?></span>
    <span class="text primary-text" id="course-start"><?= $start_date?></span>
    <span class="text primary-text" id="course-end"><?= $end_date ?></span>
    <span class="text primary-text" id="exam-grade"><?= $student->grade_obtained?></span>
    <span class="text primary-text" id="certificate-number">C/<?= $student->certificate_number ?></span>
    <span class="text primary-text" id="certificate-date"><?= $completed_at ?></span>
</page>

<button id="print" onclick="startPrint()">Print</button>
<button id="print" onclick="printBlank()">Print On Blank</button>
<div id="previewBeforeDownload"></div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js" integrity="sha512-1g3IT1FdbHZKcBVZzlk4a4m5zLRuBjMFMxub1FeIRvR+rhfqHFld9VFXXBYe66ldBWf+syHHxoZEbZyunH6Idg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
<script>
    // var pdf = new jsPDF("p", "mm", "a4");
    var elementHTML = $('#contnet').html();
    function printBlank(){
        var background = $('page').css('background-image');
        $('page').css('background-image', 'none');
        $('page').css('background-color', 'transparent');
        startPrint();
        $('page').css('background-image', background);

    }
    function startPrint(){
        var name = $("#certificate-number").text()+'.pdf';
        $("page").css("box-shadow", "none");
        html2canvas(document.querySelector("#content")).then(canvas => {

            var imgData = canvas.toDataURL("image/jpeg",1);
            var pdf = new jsPDF("p", "mm", "a4");
            var pageWidth = pdf.internal.pageSize.width;
            var pageHeight = pdf.internal.pageSize.height;
            var imageWidth = canvas.width;
            var imageHeight = canvas.height;

            var ratio = imageWidth/imageHeight >= pageWidth/pageHeight ? pageWidth/imageWidth : pageHeight/imageHeight;
            pdf.addImage(imgData, 'JPEG', 0, 0, imageWidth * ratio, imageHeight * ratio);
            pdf.save(name);

            //$("#previewBeforeDownload").hide();
        });
        $("page").css("box-shadow", "0 0 0.5cm rgba(0,0,0,0.5)");
    }

</script>

</body>
</html>
