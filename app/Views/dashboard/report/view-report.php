<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


    <!-- Main Content -->
    <div id="content">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <?= $this->include('Views/components/_message_block') ?>

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-success">All Students Report Card.</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Student name</th>
                                <th>Branch Name</th>
                                <th>Course Name</th>
                                <th>Grade Obtained</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($students)){
                                foreach ($students as $student){?>
                                    <tr>
                                        <td><?= $student->name;?></td>
                                        <td><?= $student->branch_name;?></td>
                                        <td><?= $student->course_name;?></td>
                                        <td><?= $student->grade_obtained;?></td>

                                        <td>
                                            <form action="<?= route_to('generate_report')?>" method="post">
                                                <input type="hidden" name="student_id" value="<?= $student->id?>">
                                                <button type="submit" class="btn btn-info btn-small" aria-details="Edit" title="Edit Course">
                                                    <i class="fas fa-eye"></i>
                                                    <span>View Report</span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php }
                            }else{
                                echo "<tr class='text-center'> <p class='text-center'>No Data Present Here</p></tr>";
                            } ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- DataTales Example -->


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->



<?= $this->endSection() ?>