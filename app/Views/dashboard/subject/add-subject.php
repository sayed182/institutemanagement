<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

<div class="container">
    <?= $this->include('components/_message_block'); ?>
    <?php if (false) { ?>
        <div class="card w-75 mx-auto o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Add a Subject</h1>
                            </div>
                            <form class="user" action="<?= route_to('post_add_subject') ?>" method="post">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="Subject Name" name="name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Subject Code" name="code">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="email" class="form-control form-control-user" id="exampleFirstName" placeholder="Subject Author" name="author">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <select type="text" class="form-control rounded-pill" id="exampleLastName" placeholder="Subject Course" name="course_id">
                                            <option value="">Please select a course</option>
                                            <?php foreach ($courses as $course) { ?>
                                                <option value="<?= $course->id; ?>"><?= $course->name; ?></option>
                                            <?php } ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <select type="text" class="form-control rounded-pill" id="exampleLastName" placeholder="Subject Type" name="type">
                                            <option value="theory">Theory</option>
                                            <option value="practical">Practical</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Add Subject
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-md-8 col-sm-12 offset-md-2">
            <div class="card card-primary">
                <div class="card-header bg-success">
                    <h5 class="card-title">Add Courses</h5>
                </div>
                <!-- /.card-header -->
                <form class="user" action="<?= route_to('post_add_subject') ?>" method="post">
                    <div class="card-body">

                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control" id="exampleFirstName" placeholder="Subject Name" name="name">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="exampleLastName" placeholder="Subject Code" name="code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <input type="email" class="form-control" id="exampleFirstName" placeholder="Subject Author" name="author">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <select type="text" class="form-control" id="exampleLastName" placeholder="Subject Course" name="course_id">
                                    <option value="">Please select a course</option>
                                    <?php foreach ($courses as $course) { ?>
                                        <option value="<?= $course->id; ?>"><?= $course->name; ?></option>
                                    <?php } ?>

                                </select>
                            </div>

                            <div class="col-sm-6">
                                <select type="text" class="form-control" id="exampleLastName" placeholder="Subject Type" name="type">
                                    <option value="theory">Theory</option>
                                    <option value="practical">Practical</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-primary">
                            Add Course
                        </button>
                        <button type="reset" class="btn btn-danger">
                            Clear
                        </button>
                    </div>
                </form>
                <!-- form start -->

            </div>
        </div>
    <?php } ?>
</div>

<?= $this->endSection() ?>