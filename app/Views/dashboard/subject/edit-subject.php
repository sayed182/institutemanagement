<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

    <div class="container">
        <?= $this->include('components/_message_block'); ?>
        <div class="card w-75 mx-auto o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Edit Subject</h1>
                            </div>
                            <form class="user" action="<?= route_to('post_add_subject')?>" method="post">
                                <?= csrf_field()?>
                                <input type="hidden" name="id" value="<?= $subject->id; ?>">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                               placeholder="Subject Name" name="name" value="<?= $subject->name; ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="exampleLastName"
                                               placeholder="Subject Code" name="code" value="<?= $subject->code; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="email" class="form-control form-control-user" id="exampleFirstName"
                                               placeholder="Subject Author" name="author" value="<?= $subject->author; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <select type="text" class="form-control rounded-pill" id="exampleLastName"
                                                placeholder="Subject Course" name="course_id">
                                            <option value="">Please select a course</option>
                                            <?php foreach ($courses as $course){?>
                                                <option value="<?= $course->id;?>" <?= $subject->course_id == $course->id?'selected':''; ?>><?= $course->name;?></option>
                                            <?php }?>

                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <select type="text" class="form-control rounded-pill" id="exampleLastName"
                                                placeholder="Subject Type" name="type">
                                            <option value="theory" <?= $subject->type == 'theory'?'selected':''; ?>>Theory</option>
                                            <option value="practical" <?= $subject->type == 'practical'?'selected':''; ?>>Practical</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Edit Subject
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?= $this->endSection() ?>