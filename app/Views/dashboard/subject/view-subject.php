<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


    <!-- Main Content -->
    <div id="content">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <?= $this->include('components/_message_block'); ?>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">All Subjects</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Author</th>
                                <th>Type</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                            <th>Name</th>
                                <th>Code</th>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Author</th>
                                <th>Type</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            
                            <?php foreach ($subjects as $subject){?>
                                <tr>
                                    <td><?= $subject->name;?></td>
                                    <td><?= $subject->code;?></td>
                                    <td><?= $subject->course_name;?></td>
                                    <td><?= $subject->course_code;?></td>
                                    <td><?= $subject->author;?></td>
                                    <td><?= $subject->type;?></td>
                                    <td>
                                        <a href="<?= route_to('edit_subject').'?id='.$subject->id; ?>" class="btn btn-info btn-small" aria-details="Edit" title="Edit Subject">
                                            <i class="fas fa-clipboard-check"></i>
                                        </a>
                                        <form class="d-inline" action="<?= route_to('delete_subject'); ?>" method="post">
                                            <input type="hidden" name="id" value="<?= $subject->id;?>">
                                            <button type="submit" class="btn btn-danger btn-small" aria-details="Delete " title="Delete Subject">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                            <?php }?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->



<?= $this->endSection() ?>