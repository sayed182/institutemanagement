<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

    <div class="container main">
        <div class="main-body">

            <?= $this->include('components/_message_block'); ?>
            <div class="row gutters-sm">
                <div class="col-md-8 mx-auto">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Admin Name</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <?= $admin->username?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Email</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    <?= $admin->email?>
                                </div>
                            </div>
                            <form action="<?= route_to("admin_update_password")?>" class="mt-3" method="post" id="change-password">
                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0">Current Password</h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                        <input type="text" name="current_password" id="" class="form-control form-control-user" required>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-3">
                                        <h6 class="mb-0">New Password</h6>
                                    </div>
                                    <div class="col-sm-9 text-secondary">
                                        <input type="text" name="new_password" id="" class="form-control form-control-user" required>
                                    </div>
                                </div>
                            </form>

                            <hr>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-info " type="submit" form="change-password">Update Password</button>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>

        </div>
    </div>



<?= $this->endSection() ?>