<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


<!-- Main Content -->
        <div id="content">


            <!-- Begin Page Content -->
            <div class="container-fluid">

                <?= $this->include('Views/components/_message_block') ?>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">All Branches</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Branch Name</th>
                                    <th>Branch Owner Name</th>
                                    <th>Address</th>
                                    <th>Mail Id</th>
                                    <th>Contact No.</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Branch Name</th>
                                    <th>Branch Owner Name</th>
                                    <th>Address</th>
                                    <th>Mail Id</th>
                                    <th>Contact No.</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php if(!empty($branches)){
                                    foreach ($branches as $branch){?>
                                        <tr>
                                            <td><?= $branch->name?></td>
                                            <td><?= $branch->owners_name?></td>
                                            <td><?= $branch->address?></td>
                                            <td><?= $branch->email?></td>
                                            <td><?= $branch->phone_number?></td>
                                            <td>
                                                <a href="<?= route_to('edit_branch').'?id='.$branch->id; ?>" class="btn btn-info btn-small" aria-details="Edit" title="Edit Subject">
                                                    <i class="fas fa-clipboard-check"></i>
                                                </a>
                                                <form class="d-inline" action="<?= route_to('delete_branch'); ?>" method="post">
                                                    <input type="hidden" name="id" value="<?= $branch->id;?>">
                                                    <button type="submit" class="btn btn-danger btn-small" aria-details="Delete " title="Delete Subject">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                   <?php }
                                }?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->



<?= $this->endSection() ?>