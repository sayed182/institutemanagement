<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

    <div class="container main">
        <?= $this->include('Views/components/_message_block') ?>
        <div class="card w-75 mx-auto o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create a Branch</h1>
                            </div>
                            <form class="user" method="post" action="<?= route_to('post_add_branch')?>" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                               placeholder="Branch Name" name="branch-name">
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="email" class="form-control form-control-user" id="exampleFirstName"
                                               placeholder="Branch Email" name="email">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control form-control-user" id="exampleLastName"
                                               placeholder="Branch Ph." name="branch-phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName"
                                               placeholder="Owner's Name" name="owners-name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control form-control-user" id="exampleLastName"
                                               placeholder="Owner's Contact" name="owners-phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="exampleInputEmail"
                                           placeholder="Branch Address" name="address">
                                </div>
                                <div class="form-group">
                                    <input type="file" class="form-control rounded-pill" id="exampleInputEmail"
                                           placeholder="Owners picture" name="owners_picture">
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Register Account
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?= $this->endSection() ?>