<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


    <!-- Main Content -->
    <div id="content">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <?= $this->include('Views/components/_message_block') ?>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">All Branches</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Duration ( Months )</th>
                                <th>Fees</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Duration ( Months )</th>
                                <th>Fees</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php if(!empty($courses)){
                                foreach ($courses as $course){?>
                                    <tr>
                                        <td><?= $course->name;?></td>
                                        <td><?= $course->code;?></td>
                                        <td><?= $course->duration;?></td>
                                        <td>&#8377;<?= $course->fees;?></td>
                                        <td>
                                            <a href="<?= route_to('edit_course').'?id='.$course->id; ?>" class="btn btn-info btn-small" aria-details="Edit" title="Edit Course">
                                                <i class="fas fa-clipboard-check"></i>
                                            </a>
                                            <form class="d-inline" action="<?= route_to('delete_course'); ?>" method="post">
                                                <input type="hidden" name="id" value="<?= $course->id;?>">
                                                <button type="submit" class="btn btn-danger btn-small" aria-details="Delete " title="Delete Course">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php }
                            }else{
                                echo "<tr class='text-center'> <p class='text-center'>No Data Present Here</p></tr>";
                            } ?>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->



<?= $this->endSection() ?>