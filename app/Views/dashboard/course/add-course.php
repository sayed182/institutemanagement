<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

<div class="container main">
    <?= $this->include('Views/components/_message_block') ?>
    <?php if (false) { ?>
        <div class="card w-75 mx-auto o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Add a Course</h1>
                            </div>
                            <form class="user" action="<?= route_to('post_add_course'); ?>" method="post">
                                <?= csrf_field() ?>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="Course Name" name="name" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Course Code" name="code" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="number" class="form-control form-control-user" id="exampleFirstName" placeholder="Course Duration in Months" name="duration" required>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="number" class="form-control form-control-user" id="exampleFirstName" placeholder="Course Fees" name="fees" required>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Add Course
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>

        <div class="col-md-8 col-sm-12 offset-md-2">
            <div class="card card-primary">
                <div class="card-header bg-success">
                    <h5 class="card-title">Add Courses</h5>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="user" action="<?= route_to('post_add_course'); ?>" method="post">
                    <?= csrf_field() ?>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control " id="exampleFirstName" placeholder="Course Name" name="name" required>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control " id="exampleLastName" placeholder="Course Code" name="code" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="number" class="form-control" id="exampleFirstName" placeholder="Course Duration in Months" name="duration" required>
                            </div>
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="number" class="form-control" id="exampleFirstName" placeholder="Course Fees" name="fees" required>
                            </div>
                        </div>
                    </div>


                    <div class="card-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-primary">
                            Add Course
                        </button>
                        <button type="reset" class="btn btn-danger ">
                            Clear
                        </button>
                    </div>


                </form>
            </div>
        </div>

    <?php } ?>

</div>

<?= $this->endSection() ?>