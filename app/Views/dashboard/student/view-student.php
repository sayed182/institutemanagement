<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


    <!-- Main Content -->
    <div id="content">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <?= $this->include('Views/components/_message_block') ?>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">All Students</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Registration No.</th>
                                <th>Roll No.</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Registration No.</th>
                                <th>Roll No.</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php if(!empty($students)){
                                foreach ($students as $student){?>
                                    <tr>
                                        <td><?= $student->name;?></td>
                                        <td><?= $student->name;?></td>
                                        <td><?= $student->registration_number;?></td>
                                        <td><?= $student->roll_number;?></td>
                                        <td class="text-capitalize"><?= $student->status ?></td>
                                        <td>
                                            <a href="<?= route_to('edit_student').'?id='.$student->id; ?>" class="btn btn-success btn-small" aria-details="Edit ">
                                                <i class="fas fa-clipboard-check"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php }
                            }?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->



<?= $this->endSection() ?>