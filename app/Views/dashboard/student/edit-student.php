<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

<div class="container main">
    <?= $this->include('Views/components/_message_block') ?>
    <!--        <div class="text-center">-->
    <!--            <h1 class="h4 text-gray-900 mb-1">Add a Student</h1>-->
    <!--        </div>-->
    <?php if (false) { ?>
        <div class="card w-100 mx-auto o-hidden border-0 shadow-lg mt-2 mb-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <form action="<?= route_to('post_add_student') ?>" method="post" class="user w-100">
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h6 text-gray-900 mb-1">Student Personal Details</h1>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" placeholder="Student's Name" name="name" value="<?= $student->name; ?>">
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" placeholder="Father's Name" name="father_name" value="<?= $student->father_name; ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" placeholder="Mother's Name" name="mother_name" value="<?= $student->mother_name; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <select type="text" class="form-control rounded-pill" placeholder="Gender" name="gender">
                                            <option value="male" <?= $student->gender == 'male' ? 'selected' : ''; ?>>Male</option>
                                            <option value="female" <?= $student->gender == 'female' ? 'selected' : ''; ?>>Female</option>
                                            <option value="other" <?= $student->gender == 'other' ? 'selected' : ''; ?>>Other</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="date" class="form-control form-control-user" placeholder="DOB" name="dob" value="<?= $student->dob; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="number" class="form-control form-control-user" placeholder="Contact No." name="phone_number" value="<?= $student->phone_number; ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control form-control-user" placeholder="Email" name="email" value="<?= $student->email; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="exampleInputEmail" placeholder="Address" name="address" value="<?= $student->address; ?>">
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h6 text-gray-900 mb-1">Student Academic Details</h1>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" placeholder="Registration Number" name="registration_number" value="<?= $student->registration_number; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control form-control-user" placeholder="Roll Number" name="roll_number" value="<?= $student->roll_number; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-sm-12">
                                        <input type="hidden" name="branch_id" value="<?= $student->branch_id; ?>">
                                        <input type="Text" class="form-control form-control-user" placeholder="Center Code" name="branch_code" value="<?= $student->branch_code ?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <select type="text" class="form-control rounded-pill" id="courseName" placeholder="Course Name" name="course_id" required>
                                            <option value="">Please select a course</option>
                                            <?php if (!empty($courses)) {
                                                foreach ($courses as $course) { ?>
                                                    <option value="<?= $course->id; ?>"><?= $course->name; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <select type="text" class="form-control rounded-pill" id="subjectName" placeholder="Subject Name" name="subject_id" required>
                                        </select>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="col-lg-12 px-5 pb-5 pt-0">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Edit Student
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-md-9 col-sm-12 offset-md-1">
            <div class="card card-primary">
                <div class="card-header bg-success">
                    <h5 class="card-title">Edit Student Details</h5>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="<?= route_to('post_add_student') ?>" method="post" class="user w-100">

                    <div class="card-body">

                        <div class="text-center">
                            <h1 class="h5 text-gray-900 mb-1">Student Personal Details</h1>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <input type="text" class="form-control " placeholder="Student's Name" name="name" value="<?= $student->name; ?>">
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control " placeholder="Father's Name" name="father_name" value="<?= $student->father_name; ?>">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control " placeholder="Mother's Name" name="mother_name" value="<?= $student->mother_name; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <select type="text" class="form-control " placeholder="Gender" name="gender">
                                    <option value="male" <?= $student->gender == 'male' ? 'selected' : ''; ?>>Male</option>
                                    <option value="female" <?= $student->gender == 'female' ? 'selected' : ''; ?>>Female</option>
                                    <option value="other" <?= $student->gender == 'other' ? 'selected' : ''; ?>>Other</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <input type="date" class="form-control " placeholder="DOB" name="dob" value="<?= $student->dob; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="number" class="form-control " placeholder="Contact No." name="phone_number" value="<?= $student->phone_number; ?>">
                            </div>
                            <div class="col-sm-6">
                                <input type="email" class="form-control " placeholder="Email" name="email" value="<?= $student->email; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control " id="exampleInputEmail" placeholder="Address" name="address" value="<?= $student->address; ?>">
                        </div>

                        
                        <div class="text-center pt-4 d-block">
                            <h1 class="h5 text-gray-900 mb-1">Student Academic Details</h1>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="student_roll">Student Roll</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control " placeholder="Roll Number" name="roll_number" value="<?= $student->roll_number;?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="branch_code">Branch Code</label>
                            </div>

                            <div class="col-sm-9">
                                <input type="hidden" name="branch_id" value="<?= $student->branch_id; ?>">
                                <input type="Text" class="form-control " placeholder="Center Code" name="branch_code" value="<?= $student->branch_code ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 d-flex align-items-center">
                                <label for="registration_number">Select Course</label>
                            </div>
                            <div class="col-sm-9 mb-3 mb-sm-0">
                                
                                <select type="text" class="form-control" id="courseName" placeholder="Course Name" name="course_id" required>
                                    <option value="">Please select a course</option>
                                    <?php if (!empty($courses)) {
                                        foreach ($courses as $course) { ?>
                                            <option value="<?= $course->id; ?>" data-code="<?= $course->code; ?>" <?= $student->course_id == $course->id?'selected':''; ?> ><?= $course->name; ?></option>
                                    <?php }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="registration_number">Registration Number</label>
                            </div>
                            <div class="col-sm-9 mb-3 mb-sm-0">
                                <div class="row d-flex align-items-center">
                                    <div class="col-3">
                                    <input type="text" class="form-control " value="<?= $student->branch_code ?>" disabled>
                                    </div>
                                    <span>/</span>
                                    <div class="col-3">
                                    <input type="text" class="form-control " id="reg_branch_code" value="" disabled>
                                    </div>
                                    <span>/</span>
                                    <div class="col-4">
                                    <input type="text" class="form-control " value="" id="reg_post" required>        
                                    </div>
                                </div>
                                <input type="hidden" class="form-control " name="registration_number" value="<?= $student->registration_number; ?>" required>
                            </div>
                        </div>




                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-primary">
                            Save Student
                        </button>
                        <button type="reset" class="btn btn-danger">
                            Clear
                        </button>
                    </div>

                </form>
            </div>
        </div>
    <?php } ?>

</div>

<?= $this->endSection() ?>

<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('scripts') ?>
<script>
    $("#courseName").on('change', function(event){
        var subject_code = ($("#courseName :selected").data('code'));
        $("#reg_branch_code").val(subject_code);
        var reg_pre=`<?= $student->branch_code ?>/${subject_code}/${$("#reg_post").val()}`;
        $("input[name=registration_number]").val(reg_pre);
    })
    $("#reg_post").on('keyup',function(){
        var reg= `<?= $student->branch_code ?>/${$("#reg_branch_code").val()}/`;
        var te = $("#reg_post").val();
        $("input[name=registration_number]").val(reg+te);
    });
    $("#courseName").on('change', function(event) {
        console.log(event.target.value);
        $.ajax({
            type: 'post',
            url: "<?= route_to('get_subjects') ?>",
            data: {
                'id': event.target.value
            },
            success: function(response) {
                console.log(response);
                var data = JSON.parse(response);
                var container = $("#subjectName");
                data.forEach((element) => {
                    container.append(`
                        <option value="${element.id}">${element.name}</option>
                    `)
                })
            },
            error: function(error) {
                console.log(error);
            }
        })
    })
</script>
<?= $this->endSection() ?>