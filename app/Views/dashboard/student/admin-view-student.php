<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


    <!-- Main Content -->
    <div id="content">


        <!-- Begin Page Content -->
        <div class="container-fluid">

            <?= $this->include('Views/components/_message_block') ?>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">All Students</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Registration No.</th>
                                <th>Roll No.</th>
                                <th>Branch Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Registration No.</th>
                                <th>Roll No.</th>
                                <th>Branch Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php if(!empty($students)){
                                foreach ($students as $student){?>
                                    <tr>
                                        <td><?= $student->name;?></td>
                                        <td><?= $student->course_name;?></td>
                                        <td><?= $student->registration_number;?></td>
                                        <td><?= $student->roll_number;?></td>
                                        <td><?= $student->branch_name;?></td>
                                        <?php if($student->status == 'completed'){?>
                                        <td class="text-capitalize"> <p class="text-danger text-center">COMPLETED</p></td>
                                        <?php }else{ ?>
                                        <td class="text-capitalize">
                                            <div class="row">
                                                <div class="col-9">
                                                    <form action="<?= route_to('admin_change_student_status')?>" method="post" class="d-inline" id="changeStudentStatusForm<?= $student->id;?>">
                                                        <input type="hidden" name="student_id" value="<?= $student->id;?>">
                                                    </form>
                                                    <select name="status" class="form-control border-bottom h-auto" form="changeStudentStatusForm<?= $student->id;?>">
                                                        <option value="pending" <?= $student->status == 'pending'?'selected':'' ?> >pending</option>
                                                        <option value="continue" <?= $student->status == 'continue'?'selected':'' ?> >continue</option>
                                                        <option value="discontinue" <?= $student->status == 'discontinue'?'selected':'' ?>>dis-continue</option>
                                                        <option value="terminated" <?= $student->status == 'terminated'?'selected':'' ?>>terminated</option>
                                                    </select>
                                                </div>
                                                <div class="col-3">
                                                    <button type="submit" form="changeStudentStatusForm<?= $student->id;?>" class="btn btn-success btn-small" aria-details="Change Status">
                                                        <i class="fas fa-check-circle"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                        <?php } ?>
                                        <td>
                                            <a href="<?= route_to('admin_edit_student').'?id='.$student->id; ?>" class="btn btn-info btn-small" aria-details="Edit Student">
                                                <i class="fas fa-clipboard-check"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php  }
                            }?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->



<?= $this->endSection() ?>