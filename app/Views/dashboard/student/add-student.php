<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

<div class="container main">
    <?= $this->include('Views/components/_message_block') ?>
    <!--        <div class="text-center">-->
    <!--            <h1 class="h4 text-gray-900 mb-1">Add a Student</h1>-->
    <!--        </div>-->
    <?php if (false) { ?>
        <div class="card w-100 mx-auto o-hidden border-0 shadow-lg mt-2 mb-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <form action="<?= route_to('post_add_student') ?>" method="post" class="user w-100">
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h6 text-gray-900 mb-1">Student Personal Details</h1>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" placeholder="Student's Name" name="name">
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" placeholder="Father's Name" name="father_name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" placeholder="Mother's Name" name="mother_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <select type="text" class="form-control rounded-pill" placeholder="Gender" name="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="date" class="form-control form-control-user" placeholder="DOB" name="dob">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="number" class="form-control form-control-user" placeholder="Contact No." name="phone_number">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" placeholder="Email" name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user" id="exampleInputEmail" placeholder="Address" name="address">
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h6 text-gray-900 mb-1">Student Academic Details</h1>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" placeholder="Registration Number" name="registration_number" value="<?= $branch_code ?>/<?= $roll; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control form-control-user" placeholder="Roll Number" name="roll_number" value="<?= $roll; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <div class="col-sm-12">
                                        <input type="hidden" name="branch_id" value="<?= $branch_id; ?>">
                                        <input type="Text" class="form-control form-control-user" placeholder="Center Code" name="branch_code" value="<?= $branch_code ?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <select type="text" class="form-control rounded-pill" id="courseName" placeholder="Course Name" name="course_id" required>
                                            <option value="">Please select a course</option>
                                            <?php if (!empty($courses)) {
                                                foreach ($courses as $course) { ?>
                                                    <option value="<?= $course->id; ?>"><?= $course->name; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="col-lg-12 px-5 pb-5 pt-0">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Register Student
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-md-9 col-sm-12 offset-md-1">
            <div class="card card-primary">
                <div class="card-header bg-success">
                    <h5 class="card-title">Register Student</h5>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="<?= route_to('post_add_student') ?>" method="post" class="user w-100">



                    <div class="card-body">
                        <div class="text-center">
                            <h1 class="h5 text-gray-900 mb-1">Student Personal Details</h1>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <input type="text" class="form-control " placeholder="Student's Name" name="name">
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control " placeholder="Father's Name" name="father_name">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control " placeholder="Mother's Name" name="mother_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <select type="text" class="form-control " placeholder="Gender" name="gender">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <input type="date" class="form-control " placeholder="DOB" name="dob">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="number" class="form-control " placeholder="Contact No." name="phone_number">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control " placeholder="Email" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control " id="exampleInputEmail" placeholder="Address" name="address">
                        </div>




                        <div class="text-center pt-4 d-block">
                            <h1 class="h5 text-gray-900 mb-1">Student Academic Details</h1>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="student_roll">Student Roll</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control " placeholder="Roll Number" name="roll_number" value="<?= $roll; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="branch_code">Branch Code</label>
                            </div>

                            <div class="col-sm-9">
                                <input type="hidden" name="branch_id" value="<?= $branch_id; ?>">
                                <input type="Text" class="form-control " placeholder="Center Code" name="branch_code" value="<?= $branch_code ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 d-flex align-items-center">
                                <label for="registration_number">Select Course</label>
                            </div>
                            <div class="col-sm-9 mb-3 mb-sm-0">
                                <select type="text" class="form-control" id="courseName" placeholder="Course Name" name="course_id" required>
                                    <option value="">Please select a course</option>
                                    <?php if (!empty($courses)) {
                                        foreach ($courses as $course) { ?>
                                            <option value="<?= $course->id; ?>" data-code="<?= $course->code; ?>"><?= $course->name; ?></option>
                                    <?php }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="registration_number">Registration Number</label>
                            </div>
                            <div class="col-sm-9 mb-3 mb-sm-0">
                                <div class="row d-flex align-items-center">
                                    <div class="col-3">
                                    <input type="text" class="form-control " value="<?= $branch_code ?>" disabled>
                                    </div>
                                    <span>/</span>
                                    <div class="col-3">
                                    <input type="text" class="form-control " id="reg_branch_code" value="" disabled>
                                    </div>
                                    <span>/</span>
                                    <div class="col-4">
                                    <input type="text" class="form-control " value="<?= $roll; ?>">        
                                    </div>
                                </div>
                                <input type="hidden" class="form-control " name="registration_number" value="<?= $roll; ?>" required>
                            </div>
                        </div>

                    </div>



                    <div class="card-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-primary">
                            Register Student
                        </button>
                        <button type="reset" class="btn btn-danger">
                            Clear
                        </button>
                    </div>


                </form>
            </div>
        </div>
    <?php } ?>

</div>

<?= $this->endSection() ?>

<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('scripts') ?>
<script>
    $("#courseName").on('change', function(event){
        var subject_code = ($("#courseName :selected").data('code'));
        $("#reg_branch_code").val(subject_code);
    })
</script>
<?= $this->endSection() ?>