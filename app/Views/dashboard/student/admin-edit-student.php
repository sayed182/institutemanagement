<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>


<div class="container">
    <div class="main-body">

        <?= $this->include('Views/components/_message_block') ?>
        <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                            <div class="mt-3">
                                <h4><?=$student->name?></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <form action="<?= route_to('admin_post_add_student')?>" class="admin-student-edit" method="post">
                        <input type="hidden" name="id" value="<?=$student->id?>">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Student Name</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" name="name" value="<?= $student->name?>" disabled>

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" name="email" value="<?= $student->email?>" disabled>

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Phone</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" name="phone_number" value="<?= $student->phone_number?>" disabled>

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Date Of Birth</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" name="dob" value="<?= $student->dob?>" disabled>

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3 ">
                                <h6 class="mb-0">Address</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                <input type="text" class="form-control" name="address" value="<?= $student->address?>" disabled>

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12" id="enableEdit">
                                <a class="btn btn-info " href="javascript:void(0)" >Edit</a>
                            </div>
                            <div class="col-sm-12 d-none" id="disableEdit">
                                <a class="btn btn-danger " href="javascript:void(0)">Cancel</a>
                                <button class="btn btn-success "  href="#">Submit</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>
        <?php if(!empty($exam_data)){ ?>
        <div class="row p-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-title">
                        <div class="text-center">
                            <h1 class="h3 text-gray-900 my-1 py-1">Student Report Card</h1>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Full Marks</th>
                                    <th>Marks Obtained</th>
                                </tr>
                                </thead>

                                <tbody id="subjectsContainer">
                                <form action="<?= route_to('post_add_report')?>" method="post" id="edit-report">
                                    <input type="hidden" name="id" value="<?= $id; ?>">
                                </form>

                                <?php
                                    foreach ($exam_data as $data){?>
                                        <tr>
                                            <td class="w-50"><input type="hidden" form="edit-report" name="subject_id[]" value="<?= $data->subject_id; ?>"><?= $data->subject_name; ?></td>
                                            <td ><input type="number" class="w-25 report-edit" form="edit-report" name="full_marks[]" id="" disabled value="<?= $data->full_marks; ?>"></td>
                                            <td ><input type="number" class="w-25 report-edit" form="edit-report" name="marks_obtained[]" id=""  disabled value="<?= $data->marks_obtained; ?>"></td>
                                        </tr>
                                    <?php }
                                ?>
                                <tr><td></td>
                                    <td>Grade Obtained : </td>
                                    <td><p class="text-bold"><?=$grade; ?></p></td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>

                    <div class="card-footer">

                        <?php if($student->status == 'completed'){?>
                            <button class="btn btn-danger float-right">Student is marked as completed.</button>
                        <?php }else{ ?>
                            <button type="submit" class="btn btn-info" id="editMarks"> Edit marks</button>
                            <div class=" d-none" id="disableReportEdit">
                                <a class="btn btn-danger " href="javascript:void(0)">Cancel</a>
                                <button type="submit" class="btn btn-success" form="edit-report">Submit</button>
                            </div>
                            <form action="<?= route_to('admin_change_student_status')?>" method="post" class="d-inline" id="changeStudentStatusForm<?= $student->id;?>">
                                <input type="hidden" name="student_id" value="<?= $student->id;?>">
                                <input type="hidden" name="status" value="completed">
                                <button type="submit" class="btn btn-success float-right">Mark, as Completed</button>
                            </form>
                        <?php } ?>
                    </div>
                </div>

            </div>

        </div>
        <?php }?>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('styles') ?>
<style>
    .admin-student-edit .row .col-sm-3{
        display: flex;
        align-items: center;
    }
</style>
<?= $this->endSection() ?>



<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('scripts') ?>
<script>
    $('#enableEdit > a').on('click', function (e){
        $("#editMarks").hide();
        $("#disableEdit").removeClass('d-none');
        $(".form-control").each((index, ele)=>{
            $(ele).removeAttr('disabled');
        })
    });
    $('#disableEdit > a').on('click', function (e){
        $("#enableEdit").show();
        $("#disableEdit").addClass('d-none');
        $(".form-control").each((index, ele)=>{
            $(ele).attr('disabled', 'true');
        })
    });

    $('#editMarks').on('click', function (e){
        $("#editMarks").hide();
        $("#disableReportEdit").addClass('d-inline');
        $(".report-edit").each((index, ele)=>{
            $(ele).removeAttr('disabled');
        })
    });
    $('#disableEdit > a').on('click', function (e){
        $("#enableEdit").show();
        $("#disableReportEdit").removeClass('d-inline');
        $(".form-control").each((index, ele)=>{
            $(ele).attr('disabled', 'true');
        })
    });

    $("#courseName").on('change', function (event){
        console.log(event.target.value);
        $.ajax({
            type:'post',
            url: "<?= route_to('get_subjects')?>",
            data:{'id': event.target.value},
            success: function (response){
                console.log(response);
                var data = JSON.parse(response);
                var container = $("#subjectName");
                data.forEach((element)=>{
                    container.append(`
                        <option value="${element.id}">${element.name}</option>
                    `)
                })
            },
            error: function (error){
                console.log(error);
            }
        })
    })
</script>
<?= $this->endSection() ?>
