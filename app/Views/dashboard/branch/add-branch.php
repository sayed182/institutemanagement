<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('main') ?>

<div class="container main">
    <?= $this->include('Views/components/_message_block') ?>
    <?php if (false) { ?>
        <div class="card w-75 mx-auto o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create a Branch</h1>
                            </div>
                            <form class="user" method="post" action="<?= route_to('post_add_branch') ?>" enctype="multipart/form-data">
                                <?= csrf_field() ?>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <input type="text" class="form-control " placeholder="Branch Name" name="branch-name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <select name="" id="branch_state" class="form-control" onchange="clearBranchCode()">
                                            <option value="OD" selected>ODISHA</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="" id="branch_district" class="form-control">
                                            <option value="AN" selected>Angul</option>
                                            <option value="BD" selected>Boudh</option>
                                            <option value="BH" selected>Bhadrak</option>
                                            <option value="BL" selected>Balangir</option>
                                            <option value="BR" selected>Bargarh</option>
                                            <option value="BW" selected>Balasore</option>
                                            <option value="CU" selected>Cuttack</option>
                                            <option value="DE" selected>Debagarh</option>
                                            <option value="KN" selected>Dhenkanal</option>
                                            <option value="GN" selected>Ganjam</option>
                                            <option value="GP" selected>Gajapati</option>
                                            <option value="JH" selected>Jharsuguda</option>
                                            <option value="JP" selected>Jajapur</option>
                                            <option value="JS" selected>Jagatsinghpur</option>
                                            <option value="KH" selected>Khordha</option>
                                            <option value="KJ" selected>Kendujhar</option>
                                            <option value="KL" selected>Kalahandi</option>
                                            <option value="KD" selected>Kandhamal</option>
                                            <option value="KO" selected>Koraput</option>
                                            <option value="KP" selected>Kendrapara</option>
                                            <option value="ML" selected>Malkangiri</option>
                                            <option value="MY" selected>Mayurbhanj</option>
                                            <option value="NB" selected>Nabarangpur</option>
                                            <option value="NU" selected>Nuapada</option>
                                            <option value="NY" selected>Nayagarh</option>
                                            <option value="PU" selected>Puri</option>
                                            <option value="RA" selected>Rayagada</option>
                                            <option value="SA" selected>Sambalpur</option>
                                            <option value="SO" selected>Subarnapur</option>
                                            <option value="SU" selected>Sundergarh</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="username" id="branch_code" required>
                                        <input type="text" class="form-control " id="branch_code_placeholder" placeholder="Branch Code" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="email" class="form-control " placeholder="Branch Email" name="email">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control " placeholder="Branch Ph." name="branch-phone">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control " placeholder="Owner's Name" name="owners-name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control " placeholder="Owner's Contact" name="owners-phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control " placeholder="Branch Address" name="address">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control " placeholder="Password" name="password">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control " placeholder="Repeat Password" name="pass_confirm">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="file" class="form-control" placeholder="Owners picture" name="owners_picture">
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Register Account
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="col-md-9 col-sm-12 offset-md-1">
            <div class="card card-primary">
                <div class="card-header bg-success">
                    <h5 class="card-title">Create Branch</h5>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="user" method="post" action="<?= route_to('post_add_branch') ?>" enctype="multipart/form-data">
                    <?= csrf_field() ?>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <input type="text" class="form-control " placeholder="Branch Name" name="branch-name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <select name="" id="branch_state" class="form-control" onchange="clearBranchCode()">
                                    <option value="OD" selected>ODISHA</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="" id="branch_district" class="form-control">
                                    <option value="AN" selected>Angul</option>
                                    <option value="BD" selected>Boudh</option>
                                    <option value="BH" selected>Bhadrak</option>
                                    <option value="BL" selected>Balangir</option>
                                    <option value="BR" selected>Bargarh</option>
                                    <option value="BW" selected>Balasore</option>
                                    <option value="CU" selected>Cuttack</option>
                                    <option value="DE" selected>Debagarh</option>
                                    <option value="KN" selected>Dhenkanal</option>
                                    <option value="GN" selected>Ganjam</option>
                                    <option value="GP" selected>Gajapati</option>
                                    <option value="JH" selected>Jharsuguda</option>
                                    <option value="JP" selected>Jajapur</option>
                                    <option value="JS" selected>Jagatsinghpur</option>
                                    <option value="KH" selected>Khordha</option>
                                    <option value="KJ" selected>Kendujhar</option>
                                    <option value="KL" selected>Kalahandi</option>
                                    <option value="KD" selected>Kandhamal</option>
                                    <option value="KO" selected>Koraput</option>
                                    <option value="KP" selected>Kendrapara</option>
                                    <option value="ML" selected>Malkangiri</option>
                                    <option value="MY" selected>Mayurbhanj</option>
                                    <option value="NB" selected>Nabarangpur</option>
                                    <option value="NU" selected>Nuapada</option>
                                    <option value="NY" selected>Nayagarh</option>
                                    <option value="PU" selected>Puri</option>
                                    <option value="RA" selected>Rayagada</option>
                                    <option value="SA" selected>Sambalpur</option>
                                    <option value="SO" selected>Subarnapur</option>
                                    <option value="SU" selected>Sundergarh</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="hidden" name="username" id="branch_code" required>
                                <input type="text" class="form-control " id="branch_code_placeholder" placeholder="Branch Code" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="email" class="form-control " placeholder="Branch Email" name="email">
                            </div>
                            <div class="col-sm-6">
                                <input type="number" class="form-control " placeholder="Branch Ph." name="branch-phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control " placeholder="Owner's Name" name="owners-name">
                            </div>
                            <div class="col-sm-6">
                                <input type="number" class="form-control " placeholder="Owner's Contact" name="owners-phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control " placeholder="Branch Address" name="address">
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control " placeholder="Password" name="password">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control " placeholder="Repeat Password" name="pass_confirm">
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" laceholder="Owners picture" name="owners_picture">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>


                    </div>


                    <div class="card-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-primary">
                            Proceed, Branch Creation
                        </button>
                        <button type="reset" class="btn btn-danger">
                            Clear
                        </button>
                    </div>

                </form>
            </div>
        </div>
    <?php } ?>


</div>

<?= $this->endSection() ?>

<?= $this->extend($this->config->viewTemplate) ?>
<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        handleDistrict();
    })

    function handleDistrict() {
        $("#branch_district").on('change', function(event) {
            var val = event.target.value;
            var branch_count = "<?= $branch_count; ?>";
            console.log(<?= $branch_count; ?>);
            var d = $("#branch_state").val() + val + branch_count.padStart(3, '0');
            $('#branch_code').val(d);
            $('#branch_code_placeholder').val(d);
        })
    }

    function clearBranchCode() {
        $('#branch_code').val("");
        $('#branch_code_placeholder').val("");
    }
</script>
<?= $this->endSection() ?>